<?php

use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use rizalafani\rajaongkirlaravel\RajaOngkirFacade as RajaOngkir;

Auth::routes();
Route::group(
    [
        'middleware' =>
        [
            'auth',
            'menu',
            'action'
        ]
    ], function()
    {

        try {
            DB::connection()->getPdo();

            Route::group(['prefix' => 'dashboard'], function()
        {
            Route::match(
                [
                    'get',
                    'post'
                ], '/{code}', function($code)
                {
                    return App::make('\App\Http\Controllers\\'.ucfirst($code).'Controller')->callAction('list', []);
                });

            if(Cache::has('routing'))
                {
                    $cache_query = Cache::get('routing');
                    foreach($cache_query as $route)
                    {
                        Route::match(
                            [
                                'get',
                                'post'
                            ], $route->action_link, ucfirst($route->action_controller).'Controller@'.$route->action_function)->name($route->action_code);
                    }
                }
                else
                {
                    $cache_query = Cache::rememberForever('routing', function ()
                    {
                        return DB::table('actions')
                        ->where('action_enable', '1')
                        ->orderBy('action_sort', 'desc')
                        ->get();
                    });
                }
            });

        } catch (\Exception $e) {
               

        }
        
    });



Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'dashboard'], function()
    {

        Route::get('groups/{code}', 'UserController@groups')->name('access_group');
        Route::get('user/reset', 'UserController@resetPassword')->name('resetpassword');
        Route::post('user/change_password', 'UserController@change_password')->name('lock');
        Route::get('permision', 'HomeController@permision')->name('permision');
        Route::match(
            [
                'get',
                'post'
            ], 'user/profile', 'UserController@showProfile')->name('profile');
    });
});

Route::get('home', 'HomeController@index')->name('home');
Route::get('dashboard', 'HomeController@index');
Route::get('master', 'HomeController@master')->name('master');
Route::get('query', 'HomeController@query')->name('query');
Route::get('error', 'homeController@error')->name('error');
Route::get('cache', function(){

    if(request()->get('code')){
        return dd(Cache::get(request()->get('code')));
    }
    elseif(request()->get('delete')){
        Cache::forget(request()->get('delete'));
        Cache::flush();
    }
    $storage = \Cache::getStore(); // will return instance of FileStore
    $filesystem = $storage->getFilesystem(); // will return instance of Filesystem
    $dir = (\Cache::getDirectory());
    $keys = [];
    foreach ($filesystem->allFiles($dir) as $file1) {

        if (is_dir($file1->getPath())) {

            foreach ($filesystem->allFiles($file1->getPath()) as $file2) {
                $keys = array_merge($keys, [$file2->getRealpath() => unserialize(substr(\File::get($file2->getRealpath()), 10))]);
            }
        }
        else {
            $keys = null;
        }
    }

    return dd($keys);
})->name('error');

Route::match(['get','post'],'setting', 'ConfigurationController@website')->name('website');
Route::match(['get','post'],'contact', 'PublicController@contact')->name('contact');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/', 'PublicController@index')->name('beranda');
Route::get('/member', 'PublicController@member')->name('member');
Route::post('/process', 'PublicController@process')->name('process');
Route::post('/install', 'PublicController@install')->name('install');
Route::post('/finish', 'PublicController@finish')->name('finish');
Route::post('/notification', 'PublicController@notification')->name('notification');


Route::get('reset', 'UserController@resetRedis')->name('reset');
Route::get('reboot', 'UserController@resetRouting')->name('reboot');

Route::match(
    [
        'get',
        'post'
    ], 'produk', function()
    {
        $input = request()->get('q');
        $segment = request()->get('segment');
        $site = request()->get('site');
        $top = request()->get('top');

        $query = DB::table('products');
        $query->leftJoin('price_groups', 'products.product_id', '=', 'price_groups.product_id');
        $query->leftJoin('stocks', 'stocks.product', '=', 'products.product_id');
        $query->whereRaw('price_groups.site_id = stocks.site_id');
        $query->where('price_groups.term_of_payment_id', '=', $top);
        $query->where('price_groups.segmentasi_id', '=', $segment);
        $query->where('price_groups.site_id', '=', $site);
        $query->where("product_name", "LIKE", "%{$input}%");

        $items = array();
        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->product_id.'#'.$value->price.'#'.$value->stock,
                'text' => $value->product_name
            ];
        }

        return response()->json($items);
    })->name('produk');

Route::match(
    [
        'get',
        'post'
    ], 'customer', function()
    {
        $input = request()->get('q');
        $query = DB::table('customers');
        $query->where("customer_name", "LIKE", "%{$input}%");
        $data = $query->whereIn('user_id', function ($query)
        {
            $query->select('value')
            ->from('filters')
            ->Where('key', '=', Auth::user()->user_id);
        });

        $items = array();
        foreach($data->get() as $value)
        {
            $items[] = [
                'id' => $value->customer_id.'#'.$value->segmentasi_id.'#'.$value->term_of_payment_id.'#'.$value->site_id,
                'text' => $value->customer_name
            ];
        }

        return response()->json($items);
    })->name('customer');

Route::match(['get','post'], 'supplier', function()
{
    $input = request()->get('q');
    $query = DB::table('suppliers');
    $query->where("supplier_name", "LIKE", "%{$input}%");
    $data = $query;
    $items = array();
    foreach($data->get() as $value)
    {
        $items[] = [
            'id' => $value->supplier_id,
            'text' => $value->supplier_name
        ];
    }

    return response()->json($items);
})->name('supplier');

Route::match(['get','post'], 'production', function()
{
    $input = request()->get('q');
    $query = DB::table('productions');
    $query->where("production_name", "LIKE", "%{$input}%");
    $data = $query;
    $items = array();
    foreach($data->get() as $value)
    {
        $items[] = [
            'id' => $value->production_id,
            'text' => $value->production_name
        ];
    }

    return response()->json($items);
})->name('production');

Route::match(['get','post'], 'warehouse', function()
{
    $input = request()->get('q');
    $query = DB::table('warehouses');
    $query->where("warehouse_name", "LIKE", "%{$input}%");
    $data = $query;
    $items = array();
    foreach($data->get() as $value)
    {
        $items[] = [
            'id' => $value->warehouse_id,
            'text' => $value->warehouse_name
        ];
    }

    return response()->json($items);
})->name('warehouse');


Route::match(
    [
        'get',
        'post'
    ], 'produk_all', function()
    {
        $input = request()->get('q');
        $segmentasi = request()->get('segmentasi');
        $material = request()->get('material');

        $query = DB::table('products');
        $query->select(['products.*',DB::raw('(SUM(stocks.qty)) as jumlah')]);
        $query->leftJoin('stocks','products.product_id','=','stocks.product_code');
        $query->where('stocks.wh_type','=','IN');
        $query->where("product_name", "LIKE", "%{$input}%");
        $query->where("product_segmentasi", "=", $segmentasi);
        $query->where("product_material", "=", $material);
        $query->groupBy('products.product_id');

        $items = array();
        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->product_id.'#'.$value->product_harga_jual.'#'.$value->jumlah.'#'.$value->product_name.'#'.$value->product_weight,
                'text' => $value->product_size
            ];
        }

        return response()->json($items);
    })->name('produk_all');

Route::match(
    [
        'get',
        'post'
    ], 'unit', function()
    {
        $input = request()->get('q');

        $query = DB::table('units');
        $query->where("unit_name", "LIKE", "%{$input}%");

        $items = array();
        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->unit_id,
                'text' => $value->unit_name
            ];
        }

        return response()->json($items);
    })->name('unit');

Route::match(
    [
        'get',
        'post'
    ], 'product_segmentasi', function()
    {
        $input      = request()->get('q');
        $material   = request()->get('material');

        $query = DB::table('products');
        $query->where("product_material", "=", $material);
        $query->where("product_grouping", "LIKE", "%{$input}%");
        $query->GroupBy("product_grouping");

        $items = array();
        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->product_segmentasi,
                'text' => $value->product_grouping
            ];
        }

        return response()->json($items);
    })->name('product_segmentasi');

Route::match(
    [
        'get',
        'post'
    ], 'category', function()
    {
        $input = request()->get('q');

        $query = DB::table('categories');
        $query->where("category_name", "LIKE", "%{$input}%");

        $items = array();
        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->category_id,
                'text' => $value->category_name
            ];
        }

        return response()->json($items);
    })->name('category');

Route::match(
    [
        'get',
        'post'
    ], 'size', function()
    {
        $input = request()->get('q');

        $query = DB::table('sizes');
        $query->where("size_name", "LIKE", "%{$input}%");

        $items = array();
        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->size_id,
                'text' => $value->size_name
            ];
        }

        return response()->json($items);
    })->name('size');

Route::match(
    [
        'get',
        'post'
    ], 'bahan', function()
    {
        $input = request()->get('q');

        $query = DB::table('products');
        $query->where("product_name", "LIKE", "%{$input}%");
        $query->where("product_category", "=", "Kain");

        $items = array();
        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->product_id.'#'.$value->product_harga_jual,
                'text' => $value->product_name
            ];
        }

        return response()->json($items);
    })->name('bahan');

Route::match(
    [
        'get',
        'post'
    ], 'batik', function()
    {
        $input = request()->get('q');

        $query = DB::table('products');
        $query->where("product_name", "LIKE", "%{$input}%");
        $query->where("product_category", "=", "Batik");

        $items = array();
        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->product_id.'#'.$value->product_name,
                'text' => $value->product_name
            ];
        }

        return response()->json($items);
    })->name('batik');

Route::match(
    [
        'get',
        'post'
    ], 'product_detail', function()
    {
        $input = request()->get('q');

        $query = DB::table('product_details');
        $query->where("product_detail_name", "LIKE", "%{$input}%");

        $items = array();
        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->product_detail_id.'#'.$value->product_detail_name,
                'text' => $value->product_detail_name
            ];
        }

        return response()->json($items);
    })->name('product_detail');

Route::match(
    [
        'get',
        'post'
    ], 'filter', function()
    {
        $use = new \App\User();

        $items = array();
        foreach($use->getFillable() as $value)
        {
            $items[] = [
                'id' => $value,
                'text' => $value
            ];
        }

        return response()->json($items);
    })->name('filter');

Route::match(
    [
        'get',
        'post'
    ], 'address', function()
    {
        $input = request()->get('id_customer');
        $query = DB::table('customer_details');
        $query->where("customer", "LIKE", "%{$input}%");

        $items = array();
        foreach($query->get() as $value)
        {
            if(!empty($value->category))
            {
                $teks = $value->address.' ('.$value->category.')';
            }
            else
            {
                $teks = $value->address;
            }
            $items[] = [
                'id' => $value->recid.'#'.$value->address,
                'text' => $teks
            ];
        }

        return response()->json($items);
    })->name('address');

Route::match(
    [
        'get',
        'post'
    ], 'group_all', function()
    {
        $input = request()->get('q');
        $query = DB::table('group_users');
        $query->where("group_user_name", "LIKE", "%{$input}%");
        $items = array();

        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->group_user_code,
                'text' => $value->group_user_name
            ];
        }

        return response()->json($items);
    })->name('group_all');

Route::match(
    [
        'get',
        'post'
    ], 'site_all', function()
    {
        $input = request()->get('q');
        $query = DB::table('sites');
        $query->where("site_name", "LIKE", "%{$input}%");
        $items = array();

        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->site_id,
                'text' => $value->site_name
            ];
        }

        return response()->json($items);
    })->name('site_all');

Route::match(
    [
        'get',
        'post'
    ], 'team_all', function()
    {
        $input = request()->get('q');
        $query = DB::table('users');
        $query->where("name", "LIKE", "%{$input}%");
        $items = array();

        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->email,
                'text' => $value->name
            ];
        }

        return response()->json($items);
    })->name('team_all');

Route::match(
    [
        'get',
        'post'
    ], 'group_module', function()
    {
        $input = request()->get('q');
        $query = DB::table('group_modules');
        $query->where("group_module_name", "LIKE", "%{$input}%");
        $items = array();

        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->group_module_code,
                'text' => $value->group_module_name
            ];
        }

        return response()->json($items);
    })->name('group_module');

Route::match(
    [
        'get',
        'post'
    ], 'po', function()
    {
        $input = request()->get('q');
        $query = DB::table('purchase_orders');
        $query->where("purchase_id", "LIKE", "%{$input}%");
        $query->where('purchase_status','!=','COMPLETED');
        $items = array();

        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->purchase_id,
                'text' => $value->purchase_id
            ];
        }

        return response()->json($items);
    })->name('po');


Route::match(
    [
        'get',
        'post'
    ], 'so', function()
    {
        $input = request()->get('q');
        $query = DB::table('orders');
        $query->where("order_id", "LIKE", "%{$input}%");
        $query->where('order_status','!=','COMPLETED');
        $items = array();

        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->order_id,
                'text' => $value->order_id
            ];
        }

        return response()->json($items);
    })->name('so');

Route::match(
    [
        'get',
        'post'
    ], 'spk', function()
    {
        $input = request()->get('q');
        $query = DB::table('spk');
        $query->where("spk_id", "LIKE", "%{$input}%");
        $query->where('spk_status','!=','COMPLETED');
        $items = array();

        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->spk_id,
                'text' => $value->spk_id
            ];
        }

        return response()->json($items);
    })->name('spk');

Route::match(
    [
        'get',
        'post'
    ], 'fee', function()
    {
        $input = request()->get('q');
        $query = DB::table('users');
        $query->where("email", "LIKE", "%{$input}%");
        $query->where('group_user','=','sales');
        $items = array();

        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->email,
                'text' => $value->name
            ];
        }

        return response()->json($items);
    })->name('fee');

Route::match(
    [
        'get',
        'post'
    ], 'do', function()
    {
        $input = request()->get('q');
        $query = DB::table('orders');
        $query->where("airbill", "LIKE", "%{$input}%");
        $query->where('order_status','=','DELIVERED');
        $items = array();

        foreach($query->get() as $value)
        {
            $items[] = [
                'id' => $value->airbill,
                'text' => $value->airbill.' ('.$value->courier_service.' )'
            ];
        }

        return response()->json($items);
    })->name('do');
// public route

Route::match(['get','post'],'install', 'PublicController@install')->name('install');

//==========================================================================


Route::match(
    [
        'get',
        'post'
    ], 'ongkir', function()
    {
        $from = request()->get('from');
        $to = request()->get('to');
        $weight = request()->get('weight');
        $courier = request()->get('courier');


       $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://pro.rajaongkir.com/api/cost",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "origin=$from&originType=city&destination=$to&destinationType=city&weight=$weight&courier=$courier",
          CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: 2b17656e6964f1bdf59dc0f109475ead"
          ),
        ));

        $response = curl_exec($curl);
        
        $parse = json_decode($response,true);
        if(isset($parse)){

            $data = $parse['rajaongkir'];
            if($data['status']['code'] == '200'){

                $items = array();
                foreach($data['results'][0]['costs'] as $value)
                {
                    $items[] = [
                        'id' => $value['cost'][0]['value'].'#'.$value['service'].' - '.$value['description'].' ('.$value['cost'][0]['etd'].' )',
                        'text' => $value['service'].' - '.number_format($value['cost'][0]['value']).' ( '.$value['cost'][0]['etd'].' )'
                    ];
                }
            }
            else{

                $items[] = [
                    'id' => null,
                    'text' => $data['status']['code'].' - '.$data['status']['description'] 
                ];
            }
        }
        else{

             $items[] = [
                    'id' => null,
                    'text' => 'Connection Time Out !'
             ];
        }
        
        curl_close($curl);

    return response()->json($items);
})->name('ongkir');

//==========================================================================    
