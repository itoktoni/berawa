<!DOCTYPE html>
<html lang="en">
@include('frontend.'.config('website.frontend').'.meta')
@yield('css')
<body>
<div class="super_container">
  
  <!-- Header -->

  <header class="header">

    <!-- Main Navigation -->

    <nav class="main_nav">
      <div class="container">
        <div class="row">
          <div class="col main_nav_col d-flex flex-row align-items-center justify-content-start">
            <div class="logo_container">
              <div class="logo">
                <a href="{{ route('beranda') }}#">
                  <img src="{{ Helper::files('logo/'.config('website.logo')) }}" alt="">
                </a>
              </div>
            </div>
            <div class="main_nav_container ml-auto">
              <ul class="main_nav_list">
                <li class="main_nav_item"><a href="{{ route('beranda') }}#">home</a></li>
                <li class="main_nav_item"><a href="{{ route('beranda') }}#about">about us</a></li>
                <li class="main_nav_item"><a href="{{ route('beranda') }}#offer">offers</a></li>
                <li class="main_nav_item"><a href="{{ route('contact') }}">contact</a></li>
                <li class="main_nav_item"><a style="color: #b3585e;" href="{{ route('member') }}">join member</a></li>
              </ul>
            </div>

            <div class="hamburger">
              <i class="fa fa-bars trans_200"></i>
            </div>
          </div>
        </div>
      </div>  
    </nav>

  </header>

  <div class="menu trans_500">
    <div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
      <div class="menu_close_container"><div class="menu_close"></div></div>
      <div class="logo menu_logo"><a href="{{ route('beranda') }}#"></a></div>
      <ul>
        <li class="menu_item"><a href="{{ route('beranda') }}#">home</a></li>
        <li class="menu_item"><a href="{{ route('beranda') }}#">about us</a></li>
        <li class="menu_item"><a href="{{ route('beranda') }}#offer">offers</a></li>
        <li class="menu_item"><a href="{{ route('contact') }}">contact</a></li>
        <li class="menu_item"><a style="color: #b3585e;" href="{{ route('member') }}">join member</a></li>
      </ul>
    </div>
  </div>

  @yield('content')

  @include('frontend.'.config('website.frontend').'.footer')

</div>
@include('frontend.'.config('website.frontend').'.script')
@yield('js')
</body>
</html>