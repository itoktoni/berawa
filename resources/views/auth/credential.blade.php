<!doctype html>
<html class="fixed">
<head>

    <meta charset="UTF-8">
    <meta name="keywords" content="{{ config('app.name', 'Laravel') }}" />
    <meta name="description" content="{{ config('app.name', 'Laravel') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('website.name', 'Laravel') }}</title>
    <link href="{{ Helper::files('logo/'.config('website.favicon','default_favicon.png')) }}" rel="shortcut icon">

    <link rel="stylesheet" href="{{ Helper::backend('vendor/bootstrap/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ Helper::backend('vendor/font-awesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ Helper::backend('vendor/magnific-popup/magnific-popup.css') }}" />

    <link rel="stylesheet" href="{{ Helper::backend('stylesheets/theme.css') }}" />
    <link rel="stylesheet" href="{{ Helper::backend('stylesheets/skins/default.css') }}" />
    <link rel="stylesheet" href="{{ Helper::backend('stylesheets/theme-custom.css') }}">
    <script src="{{ Helper::backend('vendor/modernizr/modernizr.js') }}"></script>

    <script> window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token(),]);?></script>

</head>
<body>
<section class="body-sign">
    <div class="center-sign">
       
    @yield('content')
    </div>
</section>

<!-- Vendor -->
<script src="{{ Helper::backend('vendor/jquery/jquery.js') }}"></script>
<script src="{{ Helper::backend('vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
<script src="{{ Helper::backend('vendor/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ Helper::backend('vendor/nanoscroller/nanoscroller.js') }}"></script>
<script src="{{ Helper::backend('vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>

</body>
</html>