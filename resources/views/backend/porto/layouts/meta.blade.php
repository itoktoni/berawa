
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="csrf-token" value="{{ csrf_token() }}">
<title>{{ config('app.name') }}</title>
<link type="image/x-icon" href="{{ Helper::files('logo/'.config('website.favicon','default_favicon.png')) }}" rel="shortcut icon">
 <link href="{{ Helper::files('logo/'.config('website.favicon','default_favicon.png')) }}" rel="shortcut icon">
<meta name="keywords" content="{{ config('app.name') }}" />
<meta name="description" content="{{ config('app.name') }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="stylesheet" href="{{ Helper::backend('vendor/bootstrap/css/bootstrap.min.css') }}"/>
<link rel="stylesheet" href="{{ Helper::backend('vendor/font-awesome/css/font-awesome.min.css') }}"/>
<link rel="stylesheet" href="{{ Helper::backend('vendor/pnotify/pnotify.custom.css') }}" />
<link rel="stylesheet" href="{{ Helper::backend('vendor/jquery-ui/css/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ Helper::backend('vendor/select/select2.css') }}">
<link rel="stylesheet" href="{{ Helper::backend('vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{ Helper::backend('vendor/magnific-popup/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ Helper::backend('vendor/jquery-typeahead/jquery.typeahead.min.css') }}" />

<link rel="stylesheet" href="{{ Helper::backend('stylesheets/theme.min.css') }}"/>
<link rel="stylesheet" href="{{ Helper::backend('stylesheets/skins/default.min.css') }}"/>
<link rel="stylesheet" href="{{ Helper::backend('vendor/summernote/summernote-bs4.css') }}" />

<link rel="stylesheet" href="{{ Helper::backend('stylesheets/theme-custom.css') }}">
@yield('css')

