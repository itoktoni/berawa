@extends('backend.'.config('website.backend').'.layouts.app')

@section('css')
<link rel="stylesheet" href="{{ Helper::asset('/assets/vendor/jquery-ui/css/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ Helper::asset('/assets/vendor/select/select2.css') }}">
@endsection

@section('js')
<script src="{{ Helper::asset('/assets/vendor/jquery-ui/js/jquery-ui.min.js') }}"></script>
<script src="{{ Helper::asset('/assets/vendor/select/select2.min.js') }}"></script>
@endsection

@section('javascript')

<script>

$(function () {
    $("#datepicker").datepicker({
        dateFormat: 'yy-mm-dd'
    });

    $('#site').select2({
        placeholder: 'Select an site',
        ajax: {
            url: '{{ route("site_all") }}',
            dataType: 'json',
            data: function (params) {
                return {
                    q: params.term, // search term
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    $('#group').select2({
        placeholder: 'Select an group',
        ajax: {
            url: '{{ route("group_all") }}',
            dataType: 'json',
            data: function (params) {
                return {
                    q: params.term, // search term
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    $('#team').select2({
        placeholder: 'Select an team',
        ajax: {
            url: '{{ route("team_all") }}',
            dataType: 'json',
            data: function (params) {
                return {
                    q: params.term, // search term
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    $('#sales').select2({
        placeholder: 'Select an sales',
        ajax: {
            url: '{{ route("team_all") }}',
            dataType: 'json',
            data: function (params) {
                return {
                    q: params.term, // search term
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });


});
</script>

@endsection

@section('content')

<div class="row">
    {!! Form::model($data, ['route'=>[$form.'_update', $data->$key],'class'=>'form-horizontal','files'=>true]) !!}  
    <div class="panel panel-default" style="margin-bottom: 300px;">
        <header class="panel-heading">
            <h2 class="panel-title">Edit Users</h2>
        </header>
        @include('page.'.$template.'.form')
        
    </div>

    <div class="navbar-fixed-bottom" id="menu_action">
        <div class="text-right" style="padding:5px">
            <a href="{!! route("{$form}_list") !!}" class="btn btn-warning">Back</a>
            <button type="reset" class="btn btn-default">Reset</button>
            @isset($update)
            <button type="submit" class="btn btn-primary">Save</button>
            @endisset
        </div>
    </div>

    {!! Form::close() !!}
</div>
@endsection


