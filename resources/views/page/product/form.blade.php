<div class="form-group">
    <label class="col-md-2 control-label">Product Code</label>
    <div class="col-md-4 {{ $errors->has('code') ? 'has-error' : ''}}">
        {!! Form::text('code', null, ['class' => 'form-control']) !!}
    </div>
    
    <label class="col-md-2 control-label">Product Name</label>
    <div class="col-md-4 {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<hr>

<div class="form-group">
    <label class="col-md-2 control-label">Cost Product</label>
    <div class="col-md-4 {{ $errors->has('harga_beli') ? 'has-error' : ''}} {{ $errors->has('harga_beli') ? 'has-error' : ''}}">
        {!! Form::number('harga_beli', null, ['class' => 'form-control', 'placeholder' => 'Harga Beli']) !!}
    </div>

    <label class="col-md-2 control-label">Selling Price</label>
    <div class="col-md-4 {{ $errors->has('harga_jual') ? 'has-error' : ''}} {{ $errors->has('harga_beli') ? 'has-error' : ''}}">
        {!! Form::number('harga_jual', null, ['class' => 'form-control', 'placeholder' => 'Harga Jual']) !!}
    </div>
</div>

<div class="form-group">
   
    <label class="col-md-2 control-label">Product Active</label>
    <div class="col-md-4 {{ $errors->has('active') ? 'has-error' : ''}}">
        {{ Form::select('active', $status, null, ['class'=> 'form-control']) }}
    </div>
    
     <label class="col-md-2 control-label" for="inputDefault">Product Picture</label>
    <div class="col-md-4">
        {!! Form::file('images', ['class' => 'btn btn-default btn-sm btn-block']) !!}
    </div>
</div>

<input type="hidden" value="test" name="test">
<hr>

<div class="form-group">
    {!! Form::label('name', 'Product Description', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10 {{ $errors->has('description') ? 'has-error' : ''}}">
        {!! Form::textarea('description', null, ['class' => 'form-control summernote', 'rows' => '5']) !!}
    </div>  
</div>