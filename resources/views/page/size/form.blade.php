
<div class="form-group">
    <label class="col-md-2 control-label">ID</label>
    <div class="col-md-2 {{ $errors->has('size_id') ? 'has-error' : ''}}">
        {!! Form::text('size_id', null, ['class' => 'form-control']) !!}
    </div>

    <label class="col-md-2 control-label">Code</label>
    <div class="col-md-2 {{ $errors->has('size_code') ? 'has-error' : ''}}">
        {!! Form::text('size_code', null, ['class' => 'form-control']) !!}
    </div>

    <label class="col-md-2 control-label">Name</label>
    <div class="col-md-2 {{ $errors->has('product_name') ? 'has-error' : ''}}">
        {!! Form::text('size_name', null, ['class' => 'form-control']) !!}
    </div>
</div>
