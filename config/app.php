<?php
// $url_hostname  = sprintf("%s://%s%s",isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',strtolower(gethostname()),$_SERVER['REQUEST_URI']);
if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === 'off') {
    $protocol = 'http://';
} else {
    $protocol = 'https://';
}
$url_hostname = $protocol . strtolower(gethostname()) . dirname($_SERVER['PHP_SELF']);
// $url_hostname = strtolower(gethostname().dirname($_SERVER['PHP_SELF']);
return [
    'fallback_locale' => 'en',
    'name' => 'core',
    'env' => env('APP_ENV'),
    'debug' => env('APP_DEBUG', false),
    'url' => $url_hostname,
    'timezone' => 'ASIA/Jakarta',
    'locale' => 'id',
    'cipher' => 'AES-256-CBC',
    'log' => 'single',
    'log_level' => 'debug',
    'key' => env('APP_KEY'),
    'providers' => [
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        Collective\Html\HtmlServiceProvider::class,
        Yajra\Datatables\DatatablesServiceProvider::class,
        Appzcoder\CrudGenerator\CrudGeneratorServiceProvider::class,
        Thedevsaddam\LaravelSchema\LaravelSchemaServiceProvider::class,
        Barryvdh\Debugbar\ServiceProvider::class,
        // Intervention\Image\ImageServiceProvider::class,
// Barryvdh\DomPDF\ServiceProvider::class,
// Axdlee\Config\ConfigServiceProvider::class,
// Collective\Remote\RemoteServiceProvider::class,
// Milon\Barcode\BarcodeServiceProvider::class,
// App\Providers\BroadcastServiceProvider::class,
        rizalafani\rajaongkirlaravel\RajaOngkirServiceProvider::class,
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
        Way\Generators\GeneratorsServiceProvider::class,
        Axdlee\Config\ConfigServiceProvider::class,
        Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class,
        HobbIoT\Auth\CacheableAuthUserServiceProvider::class,
        Jackiedo\DotenvEditor\DotenvEditorServiceProvider::class,
        Ixudra\Curl\CurlServiceProvider::class,
    ],
    'aliases' => [
        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,
        'Form' => Collective\Html\FormFacade::class,
        'Html' => Collective\Html\HtmlFacade::class,
        'Datatables' => Yajra\Datatables\Facades\Datatables::class,
        'Helper' => App\Helpers\Helper::class,
        'DotenvEditor' => Jackiedo\DotenvEditor\Facades\DotenvEditor::class,
        'Curl'          => Ixudra\Curl\Facades\Curl::class,
        'Debugbar' => Barryvdh\Debugbar\Facade::class,
        // 'Image' => Intervention\Image\Facades\Image::class,
        // 'SSH' => Collective\Remote\RemoteFacade::class,
        // 'PDF' => Barryvdh\DomPDF\Facade::class,
        // 'BARCODE1D' => Milon\Barcode\Facades\DNS1DFacade::class,
        // 'BARCODE2D' => Milon\Barcode\Facades\DNS2DFacade::class,
        'RajaOngkir' => rizalafani\rajaongkirlaravel\RajaOngkirFacade::class,
    ],
];
