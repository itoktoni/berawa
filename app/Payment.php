<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Payment extends Model {

        protected $table      = 'payments';
        protected $primaryKey = 'payment_id';
        protected $fillable   = [
         'payment_id',
         'account_from',
         'account_to',
         'reference',
         'payment_date',
         'payment_person',
         'payment_amount',
         'payment_attachment',
         'payment_type',
         'payment_note',
         'payment_model',
         'payment_voucher',
         'payment_description',
         'approved_by',
         'approve_amount',
         'payment_status',
         'approve_date',
         'created_by',
         'created_at',
         'updated_at',
      ];
      public $searching    = 'payment_id';
      public $timestamps   = true;
      public $incrementing = false;
      public $rules        = [
          'account_from' => 'required',
      ];
      public $datatable    = [
          'payment_voucher'       => 'Payment Voucher',
          // 'account_from'       => 'From',
          'account_to'       => 'To',
          'reference'       => 'Reference',
          'payment_date'       => 'Tanggal',
          'payment_person'       => 'Person',
          'payment_amount'       => 'Amount',
          'payment_status'       => 'Status',
          'payment_type'       => 'Type',
          'payment_model'       => 'Model',
      ];

      public function simpan($code,$request,$file)
      {
        try
        {
            if (!empty($file)) {
                $name = $code . '.' . $file->extension();
                $simpen = $file->storeAs('payment', $name);
                $request['payment_attachment'] = $name;
                $request['created_by'] = Auth::user()->name;
            }
            $this->Create($request);
            session()->put('success', 'Data Has Been Added !');
        }
        catch(Exception $ex)
        {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function hapus($data)
    {
        if(!empty($data))
        {
            foreach ($data['id'] as $id) {
              
              DB::table($this->table)
              ->where(['payment_id' => $id])
              ->update(['payment_type' => 'PENDING', 'payment_status' => 'PENDING']);
            }
        }
    }

    public function ubah($id, $data)
    {
        try
        {
            $s = $this->find($id);
            $data['approved_by'] = Auth::user()->name;
            $s->update($data);
            session()->put('success', 'Data Has Been Updated !');
        }
        catch(\Exception $e)
        {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function baca($id = null)
    {
      $data = DB::table($this->table);
        if(!empty($id))
        {
            return $data->where($this->primaryKey, $id);
        }

        return $data;
    }

    public function getByReference($code = null)
    {
      $data = DB::table($this->table);
        if(!empty($code))
        {
            return $data->where('reference', $code);
        }
      return $data;  
    }

    public function remove($id){

      try{

        $this->find($id)->delete();
        session()->put('success', 'Data Has Been Updated !');

      }
      catch(\Exception $e)
      {
            session()->flash('alert-danger', $e->getMessage());
      }
    }

}
