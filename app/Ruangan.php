<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model {

        protected $table      = 'ruangan';
        protected $primaryKey = 'ruangan_id';
        protected $fillable   = [
          'ruangan_id',
          'ruangan_name',
          'id_gedung',
      ];
      public $searching    = 'ruangan_name';
      public $timestamps   = false;
      public $incrementing = false;
      public $rules        = [
          'ruangan_name' => 'required|min:3',
      ];
      public $datatable    = [
          'ruangan_id'       => 'Code',
          'gedung_name'       => 'Gedung Name',
          'ruangan_name'       => 'ruangan Name',
      ];

      public function simpan($data)
      {
        try
        {
            $this->Create($data);
            session()->put('success', 'Data Has Been Added !');
        }
        catch(Exception $ex)
        {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function hapus($data)
    {
        if(!empty($data))
        {
            $data = collect($data)->flatten()->all();
            try
            {
                $this->Destroy($data);
                session()->flash('alert-success', 'Data Has Been Deleted !');
            }
            catch(\Exception $e)
            {
                session()->flash('alert-danger', $e->getMessage());
            }
        }
    }

    public function ubah($id, $data)
    {
        try
        {
            $s = $this->find($id);
            $s->update($data);
            session()->put('success', 'Data Has Been Updated !');
        }
        catch(\Exception $e)
        {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function baca($id = null)
    {

        $select = $this->join('gedung','gedung.gedung_id','=','ruangan.id_gedung');
        $select->addSelect('ruangan.*','gedung.gedung_name');
        if(!empty($id))
        {
            return $select->where($this->primaryKey, $id);
        }
        else
        {
            return $select;
        }
    }

}
