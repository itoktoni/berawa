<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class TermofPayment extends Model{

    protected $table = 'term_of_payments';
    protected $primaryKey = 'top_id';
    protected $fillable = [
            'top_id',
            'top_name',
            'top_day',
            'top_price',
    ];
    
    public $searching = 'top_name';
    public $timestamps = false;
    public $incrementing = false;
    public $rules = [
            'top_id' => 'required|unique:term_of_payments|min:3',
            'top_name' => 'required|min:3',
            'top_day' => 'required|min:3',
            'top_price' => 'required|min:3',
    ];
    public $datatable = [
            'top_id' => 'Code',
            'top_name' => 'Name',
            'top_day' => 'Day',
            'top_price' => 'Price',
    ];

    public function simpan($data)
    {
        try
        {
            $this->Create($data);
            session()->put('success', 'Data Has Been Added !');
        }
        catch(Exception $ex)
        {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function hapus($data)
    {
        if(!empty($data))
        {
            $data = collect($data)->flatten()->all();
            try
            {
                $this->Destroy($data);
                session()->flash('alert-success', 'Data Has Been Deleted !');
            }
            catch(\Exception $e)
            {
                session()->flash('alert-danger', $e->getMessage());
            }
        }
    }

    public function ubah($id, $data)
    {
        try
        {
            $s = $this->find($id);
            $s->update($data);
            session()->put('success', 'Data Has Been Updated !');
        }
        catch(\Exception $e)
        {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function baca($id = null)
    {
        if(!empty($id))
        {
            return DB::table($this->table)->where($this->primaryKey, $id);
        }
        else
        {
            return $this->select();
        }
    }

}
