<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Order extends Model
{

    protected $table      = 'orders';
    protected $primaryKey = 'order_id';
    protected $fillable   = [
        'order_id',
        'order_reff',
        'order_date',
        'email',
        'order_attachment',
        'order_note',
        'created_at',
        'updated_at',
        'created_by',
        'product_id',
        'order_address',
        'order_status',
        'updated_by',
        'order_total',
    ];
    public $searching    = 'order_id';
    public $timestamps   = true;
    public $incrementing = false;
    public $rules        = [
        'customer_id'         => 'required',
        'order_address'       => 'required',
        'order_delivery_date' => 'required',
        'customer_id'         => 'required',
    ];
    public $datatable = [
        'order_id'            => 'No Order',
        'order_date'          => 'Tgl Order',
        'customer_name'       => 'Name Customer',
        'order_status'        => 'Status',
    ];

    public function detail($data)
    {
        try {
            DB::table('order_detail')->insert($data);
        } catch (Exception $ex) {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function simpan($code, $request, $file = null)
    {
        try {
            if (!empty($file)) {
                $name                        = $code . '.' . $file->extension();
                $request['order_attachment'] = $name;
            }

            $request['order_id']      = $code;
            $request['order_status']  = 'CONFIRM';
            $request['order_date']    = date("Y-m-d");
            $request['created_by']    = Auth::user()->username;
            $request['email']         = Auth::user()->email;
            $request['order_address'] = str_replace(array(
                "\n",
                "\r"), '', $request['order_address']);

            $this->Create($request);

            session()->put('success', 'No.' . $code . " Berhasil Disimpan");
        } catch (Exception $ex) {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function hapus($data)
    {

        if (!empty($data)) {
            $data = collect($data)->flatten()->all();
            try {
                $this->Destroy($data);
                session()->flash('alert-success', 'Data Has Been Deleted !');
            } catch (\Exception $e) {
                session()->flash('alert-danger', $e->getMessage());
            }
        }
    }

    public function ubah($id, $request, $file = null)
    {
        try {

            $s = $this->find($id);
            $s->update($request);

            $form = $request['form'];
            if ($form == 'deliver') {
                $quantity = $request['delivery'];
                $item     = $request['product'];

                for ($i = 0; $i < count($item); $i++) {

                    $qty     = $quantity[$i];
                    $product = $item[$i];

                    $total = DB::table('order_detail');
                    $total->where(['detail' => $id, 'product' => $product]);
                    $d_total = $total->get()->first();
                    $total->update([
                        'qty_release' => $qty,
                        'price_release' => $d_total->price,
                        'total_release' => $qty * $d_total->price,
                    ]);
                }
            }

            session()->flash('alert-success', 'Data Has Been Updated !');
        } catch (\Exception $e) {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function cancel($data)
    {
        try {
            foreach ($data as $a) {
                $dapet  = $this->select('order_status')->where($this->primaryKey, '=', $a)->get();
                $status = $dapet->first();
                if ($status->order_status == 'OPEN' || $dapet->order_status == 'SUBMIT') {
                    $this->where($this->primaryKey, $a)->update(['order_status' => 'CANCEL']);
                    session()->flash('alert-success', 'Order Has Been Cancel !');
                }
            }
        } catch (\Exception $e) {
            session()->flash('alert-danger', 'Cannot Cancel This Order');
        }
    }

    public function getByTop()
    {
        return $this->select('customer_id', 'customer_name');
    }

    public function getDetail($id)
    {
        $select = DB::table('order_detail');
        $select->join('orders', 'order_id', '=', 'order_detail.detail');
        $select->join('products', 'products.product_id', '=', 'order_detail.product');

        return $select->where($this->primaryKey, $id)->get();
    }

    public function baca($id = null)
    {
        $select = $this->select();
        $select->select([
            'orders.*',
            'customer_name',
            'customer_phone',
            'customer_address',
            'customer_contact',
            'customer_email',
        ]);
        $select->addSelect('users.name');

        if (!empty($id)) {
            $select->where($this->primaryKey, $id);
        }

        $select->join('users', 'users.email', '=', 'orders.email');
        return $select->join('customers', 'customers.customer_id', '=', 'orders.customer_id');
    }

    public function getReceive($id)
    {
        $select = DB::table('order_detail');
        $select->select(['order_detail.*', 'products.*', DB::raw('sum(stocks.qty) as receive')]);
        $select->leftjoin('stocks', 'stocks.product_code', '=', 'order_detail.product');
        $select->join('products', 'products.product_id', '=', 'order_detail.product');
        $select->GroupBy('product_code');
        return $select->where('detail', $id)->get();
    }

}
