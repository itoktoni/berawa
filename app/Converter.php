<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Converter extends Model {

  protected $table      = 'converters';
  protected $primaryKey = 'converter_id';
  protected $fillable   = [
    'converter_id',
    'product_from',
    'product_to',
    'product_value',
  ];
  public $searching    = 'product_to';
  public $timestamps   = true;
  public $incrementing = false;
  public $rules        = [
    'product_from' => 'required',
    'product_to' => 'required|unique:converters',
    'product_value' => 'required',
  ];
  public $datatable    = [
    'converter_id'       => 'Code',
    'product_from' => 'Code From',
    'product_to' => 'Code From',
    'product_name' => 'Product Name',
    'product_value' => 'Value From',
  ];

  public function simpan($data)
  {
    try
    {
      $this->Create($data);
      session()->put('success', 'Data Has Been Added !');
    }
    catch(Exception $ex)
    {
      session()->put('danger', $ex->getMessage());
    }
  }

  public function hapus($data)
  {
    if(!empty($data))
    {
      $data = collect($data)->flatten()->all();
      try
      {
        $this->Destroy($data);
        session()->flash('alert-success', 'Data Has Been Deleted !');
      }
      catch(\Exception $e)
      {
        session()->flash('alert-danger', $e->getMessage());
      }
    }
  }

  public function ubah($id, $data)
  {
    try
    {
      $s = $this->find($id);
      $s->update($data);
      session()->put('success', 'Data Has Been Updated !');
    }
    catch(\Exception $e)
    {
      session()->flash('alert-danger', $e->getMessage());
    }
  }

  public function baca($id = null)
  {
    if(!empty($id))
    {
      return DB::table($this->table)->join('products','products.product_id','=','converters.product_to')->where($this->primaryKey, $id);
    }
    else
    {
      return $this->select()->join('products','products.product_id','=','converters.product_to');
    }
  }

}
