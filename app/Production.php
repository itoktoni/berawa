<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Production extends Model {

    protected $table      = 'productions'; //nama table
    protected $primaryKey = 'production_id'; //nama primary key
    protected $fillable   = [//field yang mau di save ke database
      'production_id',
      'production_name',
      'production_telp',
      'production_alamat',
      'production_images',
      'production_contact',
      'email',
      'production_description',
      'production_owner',
      'production_bank_name',
        'production_bank_person',
        'production_bank_account',
        'production_bank_place',

    ];
    public $searching     = 'production_name'; //default pencarian ketika di cari
    public $timestamps    = false; //kalau mau automatic update tanggal ketika save atau edit ubah jadi true
    public $incrementing  = false; //kalau id nya mau dibuatkan otomatis dari laravel 1,2,3
    public $rules         = [//validasi https://laravel.com/docs/5.5/validation
      'production_name' => 'required|min:3',
    ];
    public $datatable     = [//field dan header buat di show ke list table 
      'production_name'   => 'Name',
      'production_owner'   => 'Owner',
      'production_telp'   => 'Telp',
      'email'           => 'Email',
    ];

    public function simpan($request, $file = null)
    {
        try
        {
            if(!empty($file)) //handle images
            {

                $name   = $request['unic'] . '.' . $file->extension();
                $simpen = $file->storeAs('production', $name);

                $request['production_images'] = $name;
            }

            $this->Create($request);
            session()->put('success', 'Data Has Been Added !');
        }
        catch(Exception $ex)
        {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function hapus($data)
    {
        if(!empty($data))
        {
            $data = collect($data)->flatten()->all();
            try
            {
                $this->Destroy($data);
                session()->flash('alert-success', 'Data Has Been Deleted !');
            }
            catch(\Exception $e)
            {
                session()->flash('alert-danger', $e->getMessage());
            }
        }
    }

    public function ubah($id, $request, $file = null)
    {
        try
        {
            if(!empty($file)) //handle images
            {
                $name   = $request['unic'] . '.' . $file->extension();
                $simpen = $file->storeAs('production', $name);

                $request['production_images'] = $name;
            }

            $s = $this->find($id);
            $s->update($request);

            session()->put('success', 'Data Has Been Updated !');
        }
        catch(\Exception $e)
        {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function baca($id = null)
    {
        if(!empty($id))
        {
            return $this->where($this->primaryKey, $id);
        }
        else
        {
            return $this->select();
        }
    }

    public function getUser(){
        $data = DB::table('users')->where('group_user', '=', 'production');
        return $data->get();
    }

}
