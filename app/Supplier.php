<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model {

    protected $table      = 'suppliers'; //nama table
    protected $primaryKey = 'supplier_id'; //nama primary key
    protected $fillable   = [//field yang mau di save ke database
      'supplier_id',
      'supplier_name',
      'supplier_telp',
      'supplier_alamat',
      'supplier_images',
      'supplier_contact',
      'email',
      'supplier_description',
      'supplier_owner',
      'supplier_bank_name',
      'supplier_bank_place',
      'supplier_bank_person',
      'supplier_bank_account',

    ];
    public $searching     = 'supplier_name'; //default pencarian ketika di cari
    public $timestamps    = false; //kalau mau automatic update tanggal ketika save atau edit ubah jadi true
    public $incrementing  = false; //kalau id nya mau dibuatkan otomatis dari laravel 1,2,3
    public $rules         = [//validasi https://laravel.com/docs/5.5/validation
      'supplier_name' => 'required|min:3',
      'supplier_owner' => 'required|min:3',
    ];
    public $datatable     = [//field dan header buat di show ke list table 
      'supplier_name'   => 'Name',
      'supplier_owner' => 'Owner',
      'supplier_telp'   => 'Telp',
      'email'           => 'Email',
    ];

    public function simpan($request, $file = null)
    {
        try
        {
            if(!empty($file)) //handle images
            {

                $name   = $request['unic'] . '.' . $file->extension();
                $simpen = $file->storeAs('supplier', $name);

                $request['supplier_images'] = $name;
            }
            
            $this->Create($request);
            session()->put('success', 'Data Has Been Added !');
        }
        catch(Exception $ex)
        {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function hapus($data)
    {
        if(!empty($data))
        {
            $data = collect($data)->flatten()->all();
            try
            {
                $this->Destroy($data);
                session()->flash('alert-success', 'Data Has Been Deleted !');
            }
            catch(\Exception $e)
            {
                session()->flash('alert-danger', $e->getMessage());
            }
        }
    }

    public function ubah($id, $request, $file = null)
    {
        try
        {
            if(!empty($file)) //handle images
            {
                $name   = $request['unic'] . '.' . $file->extension();
                $simpen = $file->storeAs('supplier', $name);

                $request['supplier_images'] = $name;
            }

            $s = $this->find($id);
            $s->update($request);

            session()->put('success', 'Data Has Been Updated !');
        }
        catch(\Exception $e)
        {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function baca($id = null)
    {
        if(!empty($id))
        {
            return $this->where($this->primaryKey, $id);
        }
        else
        {
            return $this->select();
        }
    }

    public function getUser(){
        $data = DB::table('users')->where('group_user', '=', 'supplier');
        return $data->get();
    }

}
