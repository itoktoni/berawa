<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Material extends Model {

        protected $table      = 'materials';
        protected $primaryKey = 'material_id';
        protected $fillable   = [
          'material_id',
          'material_name',
          'material_description',
      ];
      public $searching    = 'material_name';
      public $timestamps   = false;
      public $incrementing = false;
      public $rules        = [
          'material_name' => 'required|min:3',
      ];
      public $datatable    = [
          'material_id'         => 'Code',
          'material_name'       => 'Material Name',
      ];

      public function simpan($data)
      {
        try
        {
            $this->Create($data);
            session()->put('success', 'Data Has Been Added !');
        }
        catch(Exception $ex)
        {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function hapus($data)
    {
        if(!empty($data))
        {
            $data = collect($data)->flatten()->all();
            try
            {
                $this->Destroy($data);
                session()->flash('alert-success', 'Data Has Been Deleted !');
            }
            catch(\Exception $e)
            {
                session()->flash('alert-danger', $e->getMessage());
            }
        }
    }

    public function ubah($id, $data)
    {
        try
        {
            $s = $this->find($id);
            $s->update($data);
            session()->put('success', 'Data Has Been Updated !');
        }
        catch(\Exception $e)
        {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function baca($id = null)
    {
        if(!empty($id))
        {
            return DB::table($this->table)->where($this->primaryKey, $id);
        }
        else
        {
            return $this->select();
        }
    }

}
