<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Anchu\Ftp\Facades\Ftp;

class SPK extends Model {

    protected $table = 'spk';
    protected $primaryKey = 'spk_id';
    protected $fillable = [
        'spk_id',
        'spk_reff',
        'spk_delivery',
        'spk_invoice',
        'spk_date',
        'spk_delivery_date',
        'spk_invoice_date',
        'spk_receive_date',
        'spk_send_date',
        'email',
        'spk_type_delivery',
        'spk_note',
        'customer_id',
        'spk_address',
        'address_recid',
        'site_id',
        'created_by',
        'spk_status',
        'spk_attachment',
        'production_id',
        'spk_note_production',
    ];
    public $searching = 'spk_id';
    public $timestamps = true;
    public $incrementing = false;
    public $rules = [
        'production_id' => 'required',
    ];
    public $datatable = [
        'spk_id' => 'No spk',
        'spk_date' => 'Tgl spk',
        'spk_delivery_date' => 'Tgl Pengiriman',
        'production_name' => 'Name Customer',
        'spk_status' => 'Status',
    ];

    public function detail($data) {
        try {
            DB::table('spk_detail')->insert($data);
        } catch (Exception $ex) {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function simpan($code, $request, $file) {
        try {
            if (!empty($file)) {
                $name = $code . '.' . $file->extension();
                $simpen = $file->storeAs('spk', $name);
                $request['spk_attachment'] = $name;
            }
            
            $request['spk_id'] = $code;
            $request['spk_status'] = 'OPEN';
            $request['spk_date'] = date("Y-m-d");
            $request['created_by'] = Auth::user()->username;

            $this->Create($request);

            session()->put('success', 'No.' . $code . " Berhasil Disimpan");
        } catch (Exception $ex) {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function hapus($data) {

        if (!empty($data)) {
            $data = collect($data)->flatten()->all();
            try {
                $this->Destroy($data);
                session()->flash('alert-success', 'Data Has Been Deleted !');
            } catch (\Exception $e) {
                session()->flash('alert-danger', $e->getMessage());
            }
        }
    }

    public function ubah($id, $request,$file=null)
    {
        try
        {
            if (!empty($file)) {
                $name = $id . '.' . $file->extension();
                $simpen = $file->storeAs('spk', $name);
                $request['spk_attachment'] = $name;
                if($request['spk_status'] == 'OPEN'){
                    $request['spk_status'] = 'APPROVED';
                }
            }

            $s = $this->find($id);
            $s->update($request);

            $form = $request['form'];
            $item = $request['product'];

            if(isset($request['product'])){

                if($form == 'admin'){

                    $quantity = $request['qty'];
                    $harga = $request['price'];
                    $yard = $request['yard'];    

                    for($i=0; $i < count($quantity); $i++){

                        $qty = $quantity[$i];
                        $price = $harga[$i];
                        $product = $item[$i];
                        $total = $qty * $price;
                        $dyard = $yard[$i];
                        $dtotal_yard = $yard[$i] * $qty;

                        // DB::table('spk_detail')
                        // ->where(['detail' => $id, 'product' => $product])
                        // ->update(['yard_prepare' => $dtotal_yard ]);
                    }
                }
                else if($form == 'receive'){

                    $quantity = $request['receive'];

                    for($i=0; $i < count($item); $i++){

                        $qty = $quantity[$i];
                        $product = $item[$i];

                        $total = DB::table('spk_detail');
                        $total->where(['detail' => $id, 'product' => $product]);
                        $d_total = $total->get()->first();
                        $total->update([
                            'qty_receive' => $qty,
                            'price_receive' => $d_total->price,
                            'total_receive' => $qty * $d_total->price,
                        ]);

                        if($request['spk_status'] == 'RECEIVED'){
                            $pro = DB::table('products');
                            $pro->where('product_id', $product);

                            $dp = $pro->get()->first();
                            $pro->update(['product_stock' => $dp->product_stock + $qty]);

                        }
                    }
                }

                 else if($form == 'send'){

                    $quantity = $request['send'];

                    for($i=0; $i < count($item); $i++){

                        $qty = $quantity[$i];
                        $product = $item[$i];

                        // $total = DB::table('spk_detail');
                        // $total->where(['detail' => $id, 'product' => $product]);
                        // $d_total = $total->get()->first();
                        // $total->update([
                        //     'qty_receive' => $qty,
                        //     'price_receive' => $d_total->price,
                        //     'total_receive' => $qty * $d_total->price,
                        // ]);

                        if($request['spk_status'] == 'PRODUCTION'){
                            $pro = DB::table('products');
                            $pro->where('product_id', $product);

                            $dp = $pro->get()->first();
                            $pro->update(['product_stock' => $dp->product_stock - $qty]);

                        }
                    }
                }
                else{

                    $quantity = $request['qty'];
                    $harga = $request['price'];

                    for($i=0; $i < count($quantity); $i++){

                        $qty = $quantity[$i];
                        $price = $harga[$i];
                        $product = $item[$i];
                        $total = $qty * $price;

                        DB::table('spk_detail')
                        ->where(['detail' => $id, 'product' => $product])
                        ->update(['qty_prepare' => $qty, 'price_prepare' => $price, 'total_prepare' => $total]);
                    }
                }
                
            }
            
            session()->put('success', 'Data Has Been Updated !');
        }
        catch(\Exception $e)
        {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function cancel($data) {
        try {
            foreach ($data as $a) {
                $dapet = $this->select('spk_status')->where($this->primaryKey, '=', $a)->get();
                $status = $dapet->first();
                if ($status->spk_status == 'OPEN' || $dapet->spk_status == 'SUBMIT') {
                    $this->where($this->primaryKey, $a)->update(['spk_status' => 'CANCEL']);
                    session()->flash('alert-success', 'spk Has Been Cancel !');
                }
            }
        } catch (\Exception $e) {
            session()->flash('alert-danger', 'Cannot Cancel This spk');
        }
    }

    public function getByTop() {
        return $this->select('customer_id', 'customer_name');
    }

    public function getDetailOutboundSPK($id) {

        $select = DB::table('spk');
        $select->join('spk_detail', 'spk_id', '=', 'spk_detail.detail');
        $select->join('converters', 'product_to', '=', 'spk_detail.product');
        $select->join('products', 'products.product_id', '=', 'converters.product_from');

        return $select->where($this->primaryKey, $id)->get();
    }

    public function getDetail($id) {

        $select = DB::table('spk_detail');
        $select->join('spk', 'spk_id', '=', 'spk_detail.detail');
        $select->join('products', 'products.product_id', '=', 'spk_detail.product');

        return $select->where($this->primaryKey, $id)->get();
    }

    public function getReceive($id) {

        $select = DB::table('spk_detail');
        $select->select(['spk_detail.*','products.*',DB::raw('sum(stocks.qty) as receive')]);
        $select->leftjoin('stocks', 'stocks.product_code', '=', 'spk_detail.product');
        $select->join('products', 'products.product_id', '=', 'spk_detail.product');
        $select->GroupBy('spk_detail.product');
        return $select->where('stocks.reference', $id)->get();
    }

    public function baca($id = null) {
        $select = $this->select([
            'spk.*',
            'production_name',
            'production_alamat',
            'production_telp',
            'email',
            'production_bank_account',
             'production_bank_person',
            'production_bank_name',
            'production_bank_place',
            'production_owner',
        ]);
        $select->join('productions', 'spk.production_id', '=', 'productions.production_id');

        if (!empty($id)) {
            $select->where($this->primaryKey, $id);
        }

        return $select;
    }

}
