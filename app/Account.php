<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Account extends Model {

  protected $table      = 'accounts';
  protected $primaryKey = 'account_id';
  protected $fillable   = [
    'account_id',
    'account_name',
    'account_owner',
    'account_location',
    'account_number',
    'account_type',
  ];
  public $searching    = 'account_id';
  public $timestamps   = false;
  public $incrementing = false;
  public $rules        = [
    'account_name' => 'required',
    'account_owner' => 'required',
    'account_location' => 'required',
    'account_number' => 'required',
  ];
  public $datatable    = [
    'account_name'       => 'Name',
    'account_owner'       => 'Owner',
    'account_location'       => 'Location',
    'account_number'       => 'Number',
    'account_type'       => 'Type',
  ];

  public function simpan($request)
  {
    try
    {
      $this->Create($request);
      session()->put('success', 'Data Has Been Added !');
    }
    catch(Exception $ex)
    {
      session()->put('danger', $ex->getMessage());
    }
  }

  public function hapus($data)
  {
    if(!empty($data))
    {
      $data = collect($data)->flatten()->all();
      try
      {
        $this->Destroy($data);
        session()->flash('alert-success', 'Data Has Been Deleted !');
      }
      catch(\Exception $e)
      {
        session()->flash('alert-danger', $e->getMessage());
      }
    }
  }

  public function ubah($id, $data)
  {
    try
    {
      $s = $this->find($id);
      $s->update($data);
      session()->put('success', 'Data Has Been Updated !');
    }
    catch(\Exception $e)
    {
      session()->flash('alert-danger', $e->getMessage());
    }
  }

  public function baca($id = null)
  {
    $data = DB::table($this->table);
    if(!empty($id))
    {
      return $data->where($this->primaryKey, $id);
    }

    return $data;
  }
}
