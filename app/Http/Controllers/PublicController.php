<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Config;
use Jackiedo\DotenvEditor\Facades\DotenvEditor;
use Helper;
use App;
use Veritrans_Config;
use Veritrans_Snap;
use DB;

class PublicController extends Controller
{
    public function index()
    {
        $slider = new \App\Slider();
        $product = new \App\Product();
        return View('frontend.'.config('website.frontend').'.pages.homepage',[
            'slider' => $slider->baca()->get(),
            'product' => $product->baca()->get(),
        ]);
    }  

    public function member()
    {
        $slider = new \App\Slider();
        $product = new \App\Product();
        return View('frontend.'.config('website.frontend').'.pages.homepage',[
            'slider' => $slider->baca()->get(),
            'product' => $product->baca()->get(),
        ]);
    }  

    public function process(){
        
        $id = request()->get('data');
        $model = \App\Product::find($id);
        if(!empty($model)){

            Veritrans_Config::$serverKey = config('midtrans.development.server');
            // Uncomment for production environment
            // Veritrans_Config::$isProduction = true;
            Veritrans_Config::$isSanitized = true;
            Veritrans_Config::$is3ds = true;

            $autonumber = 'V' . date('y') . date('m').Helper::unic(15);
            $voucher =  Helper::code('orders', 'order_id', $autonumber, 20);

            Order::create([
                
            ]);

            // Required
            $transaction_details = array(
              'order_id' => $voucher,
              'gross_amount' => $model->harga_jual, // no decimal allowed for creditcard
            );

             // Optional
            $item1_details = array(
              'id' => $model->id,
              'price' => $model->harga_jual,
              'quantity' => 1,
              'name' => $model->name
            );

             $item_details = array ($item1_details);

             $customer_details = array(
              'first_name'    => request()->get('name'),
              'last_name'     => "",
              'email'         => request()->get('email'),
              'phone'         => request()->get('phone'),
            );

            $enable_payments = array('credit_card');

            $transaction = array(
              'enabled_payments' => $enable_payments,
              'transaction_details' => $transaction_details,
              'customer_details' => $customer_details,
              'item_details' => $item_details,
            );

            $snapToken = Veritrans_Snap::getSnapToken($transaction);
            return $snapToken;
            // die();
        }
    }

    public function finish(){

    }

    public function notification(){

    }

    public function contact()
    {
        if(request()->isMethod('POST')){

            $data = [
                'email' => request()->get('email'),
                'name' => request()->get('first_name'),
                'header' => 'Notification Information From Customer',
                'status' => request()->get('status'),
                'date' => request()->get('date'),
                'cc' => request()->get('cc'),
                'pesan' => request()->get('pesan'),
            ];

            $from = request()->get('email');
            $name = request()->get('first_name');

            $request = request()->all();
            $this->validate(request(),[
                'email' => 'email|required',
                'first_name' => 'required',
                'pesan' => 'required',
            ]);
            
            try {
                Mail::send('emails.contact', $data, function($message) use ($from,$name) {
                        $message->to(config('mail.from.address'), config('mail.from.name'));
                        $message->subject('Notification Information From Customer');
                        $message->from(config('mail.from.address'), config('mail.from.name'));
                });    
            } catch (Exception $e) {
               // return redirect()->back();
            }

            return redirect()->back();        

        }

        $site = new \App\Site();
        $user = DB::table('users')->where('group_user','=','sales')->get();

        return View('frontend.'.config('website.frontend').'.pages.contact')->with([
            'site' => $site->all(),
            'user' => $user,
        ]);
    }  

    public function install(){

          if(request()->isMethod('POST')){

            $file = DotenvEditor::load('local.env');
            $file->setKey('DB_CONNECTION', request()->get('provider'));
            $file->setKey('DB_HOST', request()->get('host'));
            $file->setKey('DB_DATABASE', request()->get('database'));
            $file->setKey('DB_USERNAME', request()->get('username'));
            $file->setKey('DB_PASSWORD', request()->get('password'));
            $file->save();
            // dd(request()->get('provider'));
            $value = DotenvEditor::getValue('DB_CONNECTION');
            // dd($value);
            $file = DotenvEditor::setKey('DB_CONNECTION', request()->get('provider'));
            $file = DotenvEditor::save();
             // Config::write('database.connections', request()->get('provider'));
            dd(request()->all());
        }
         // rename(base_path('readme.md'), realpath('').'readme.md');
         return View('welcome');
    }

    public function cara_belanja()
    {
        return View('frontend.'.config('website.frontend').'.pages.cara_belanja');
    } 

    public function marketing()
    {
        $site = new \App\Site();
        $user = DB::table('users')->where('group_user','=','sales')->get(); 
        return View('frontend.'.config('website.frontend').'.pages.marketing')->with([
            'site' => $site->all(),
            'user' => $user,
        ]);;
    }  

    public function about()
    {
        // return View('frontend.'.config('website.frontend').'.pages.about');
    }  

    public function konfirmasi()
    {
        if(request()->isMethod('POST')){

            dd(request()->all());
        }
        return View('frontend.'.config('website.frontend').'.pages.konfirmasi');
    } 

    public function catalog()
    {
        // dd(session()->all());

        if(request()->isMethod('POST')){

            $category = request()->get('category');
            $segmentasi = request()->get('segmentasi');
            $material = request()->get('material');

            if(isset($category)){
                session()->put('category', request()->get('category'));
            }
            else{
                session()->forget('category');
            }
            if(isset($segmentasi)){
                session()->put('segmentasi', request()->get('segmentasi'));
            }
            else{
                session()->forget('segmentasi');
            }
            if(isset($material)){
                session()->put('material', request()->get('material'));
            }
            else{
                session()->forget('material');
            }
        }

        $sort = request()->get('sort');
        $product = new \App\Product();
        $segmentasi = new \App\Segmentasi();
        $category = new \App\Category();
        $material = new \App\Material();
        $data_product = $product->getStock();

        if(session()->has('category')){
            $data_product->whereIn('product_category',session()->get('category'));
        }

        if(session()->has('material')){
            $data_product->whereIn('product_material',session()->get('material'));
        }

        if(session()->has('segmentasi')){
            $data_product->whereIn('product_segmentasi',session()->get('segmentasi'));
        }

        if(!empty($sort)){
            if($sort == 'new'){

                $data_product->latest();
            }
            elseif($sort == 'low'){

                $data_product->orderBy('product_harga_jual', 'asc');
            }
            elseif($sort == 'high'){

                $data_product->orderBy('product_harga_jual', 'desc');
            }
        }

        return View('frontend.'.config('website.frontend').'.pages.catalog')->with([
            'product' => $data_product->paginate(6),
            'segmentasi' => $segmentasi->baca()->get(),
            'material' => $material->baca()->get(),
            'category' => $category->baca()->get(),
        ]);
    } 

    public function single($id)
    {
        if(!empty($id)){
            $product = new \App\Product();
            $data = $product->getStock($id)->first();
            return View('frontend.'.config('website.frontend').'.pages.single')->with('data', $data);
        }

    } 

    public function segment($id)
    {
        $segmentasi = new \App\Segmentasi();
        $category = new \App\Category();
        $material = new \App\Material();
        if(!empty($id)){
            $product = new \App\Product();
            $data = $product->segment($id)->paginate(6);
            return View('frontend.'.config('website.frontend').'.pages.catalog')->with([
                'product' => $data,
                'segmentasi' => $segmentasi->baca()->get(),
                'category' => $category->baca()->get(),
                'material' => $material->baca()->get(),
            ]);
        }

    } 
}
