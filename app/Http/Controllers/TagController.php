<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Yajra\Datatables\Facades\Datatables;

class TagController extends Controller
{

    public $table;
    public $key;
    public $field;
    public $model;
    public $template;
    public $rules;
    public $datatable;
    public $searching;
    public $read;
    public $render;

    public function __construct()
    {
        $this->model = new \App\Tag();
        $this->table = $this->model->getTable();
        $this->key = $this->model->getKeyName();
        $this->field = $this->model->getFillable();
        $this->datatable = $this->model->datatable;
        $this->rules = $this->model->rules;
        $this->searching = $this->model->searching;
        $this->template  = $this->getTemplate();
        $this->render    = 'page.' . $this->template . '.';
    }

    public function index()
    {
        return redirect()->route($this->getModule() . 'list');
    }

    public function create()
    {
        //dd($this->field);
        if (request()->isMethod('POST')) {
            $this->validate(request(), $this->rules);
            $request = request()->all();
            $this->model->simpan($request);
            return redirect()->back();
        }

        return view($this->render . __function__)->with('template', $this->template);
    }

    public function list()
    {
        if (request()->isMethod('POST')) {
            $getData = $this->model->baca();
            $datatable = Datatables::of($this->filter($getData))
                ->addColumn('checkbox', function ($select) {
                    $id = $this->key;
                    $checkbox = '<input type="checkbox" name="id[]" class="chkSelect" style="margin-left:10px;" id="checkbox1" value="' . $select->$id . '">';
                    return $checkbox;
                })->addColumn('action', function ($select) {

                $id = $this->key;
                $gabung = '<div class="aksi text-center">';
                if (session()->get('akses.update')) {
                    $gabung = $gabung . '<a href="' . route($this->getModule() . '_update', [
                        'code' => $select->$id]) . '" class="btn btn-xs btn-primary">edit</a> ';
                }
                $gabung = $gabung . ' <a href="' . route(Route::currentRouteName(), [
                    'code' => $select->$id]) . '" class="btn btn-xs btn-success">show</a></div>';
                return $gabung;
            });

            if (request()->has('search')) {
                $code = request()->get('code');
                $search = request()->get('search');
                $aggregate = request()->get('aggregate');
                $datatable->where(empty($code) ? $this->searching : $code, empty($aggregate) ? 'like' : $aggregate, "%$search%");
            }

            return $datatable->make(true);
        }
        if (request()->has('code')) {
            $id = request()->get('code');
            $data = $this->model->baca($id);

            return view($this->render . '.show')->with([
                'fields' => $this->datatable,
                'data' => $this->validasi($data),
                'key' => $this->key,
                'template' => $this->template,
            ]);
        }

        return view($this->render . __function__)->with([
            'fields' => $this->datatable,
            'template' => $this->template,
        ]);
    }

    public function update()
    {
        $id = request()->get('code');
        if (!empty($id)) {
            $getData = $this->model->baca($id);
            return view($this->render . __function__)->with([
                'template' => $this->template,
                'data' => $this->validasi($getData),
                'key' => $this->key,
            ]);
        } else {
            if (request()->isMethod('POST')) {
                $id = collect(request()->query())->flip()->first();
                $requestData = request()->all();
                $this->model->ubah($id, $requestData);
            }
            return redirect()->back();
        }
    }

    public function delete()
    {
        $input = request()->all();
        $this->model->hapus($input);

        return redirect()->back();
    }

}
