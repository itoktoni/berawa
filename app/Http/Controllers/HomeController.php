<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Middleware\AccessMenu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

// use Anchu\Ftp\Facades\Ftp;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list()
    {
        return $this->index();
    }

    public function index()
    {
        $akses = new AccessMenu();
        $akses->setData($akses->getData());
        
        // $jan = DB::table('orders');
        //     $jan->join('order_detail', 'orders.order_id', '=', 'order_detail.detail');
        //     $jan = $jan->whereMonth('order_date', '=', '01');

        //     $feb = DB::table('orders');
        //     $feb->join('order_detail', 'orders.order_id', '=', 'order_detail.detail');
        //     $feb = $feb->whereMonth('order_date', '=', '02');

        //     $mar = DB::table('orders');
        //     $mar->join('order_detail', 'orders.order_id', '=', 'order_detail.detail');
        //     $mar = $mar->whereMonth('order_date', '=', '03');

        //     $apr = DB::table('orders');
        //     $apr->join('order_detail', 'orders.order_id', '=', 'order_detail.detail');
        //     $apr = $apr->whereMonth('order_date', '=', '04');

        //     $mei = DB::table('orders');
        //     $mei->join('order_detail', 'orders.order_id', '=', 'order_detail.detail');
        //     $mei = $mei->whereMonth('order_date', '=', '05');

        //     $jun = DB::table('orders');
        //     $jun->join('order_detail', 'orders.order_id', '=', 'order_detail.detail');
        //     $jun = $jun->whereMonth('order_date', '=', '06');

        //     $jul = DB::table('orders');
        //     $jul->join('order_detail', 'orders.order_id', '=', 'order_detail.detail');
        //     $jul = $jul->whereMonth('order_date', '=', '07');

        //     $agu = DB::table('orders');
        //     $agu->join('order_detail', 'orders.order_id', '=', 'order_detail.detail');
        //     $agu = $agu->whereMonth('order_date', '=', '08');

        //     $sep = DB::table('orders');
        //     $sep->join('order_detail', 'orders.order_id', '=', 'order_detail.detail');
        //     $sep = $sep->whereMonth('order_date', '=', '09');

        //     $okt = DB::table('orders');
        //     $okt->join('order_detail', 'orders.order_id', '=', 'order_detail.detail');
        //     $okt = $okt->whereMonth('order_date', '=', '10');

        //     $nov = DB::table('orders');
        //     $nov->join('order_detail', 'orders.order_id', '=', 'order_detail.detail');
        //     $nov = $nov->whereMonth('order_date', '=', '11');

        //     $des = DB::table('orders');
        //     $des->join('order_detail', 'orders.order_id', '=', 'order_detail.detail');
        //     $des = $des->whereMonth('order_date', '=', '12');

        //     $customer = DB::table('customers');
        //     $so       = DB::table('orders');
        //     $value    = DB::table('orders');

        //     $value->join('order_detail', 'orders.order_id', '=', 'order_detail.detail');
        //     $value = $value->whereMonth('order_date', '=', date('m'));

        //     $item = DB::table('orders');
        //     $item->join('order_detail', 'orders.order_id', '=', 'order_detail.detail');
        //     $item = $item->whereMonth('order_date', '=', date('m'));


        // if (Auth::user()->group_user == 'sales') {
        //     $jan->where('email', '=', Auth::user()->email);
        //     $feb->where('email', '=', Auth::user()->email);
        //     $mar->where('email', '=', Auth::user()->email);
        //     $apr->where('email', '=', Auth::user()->email);
        //     $mei->where('email', '=', Auth::user()->email);
        //     $jun->where('email', '=', Auth::user()->email);
        //     $jul->where('email', '=', Auth::user()->email);
        //     $agu->where('email', '=', Auth::user()->email);
        //     $sep->where('email', '=', Auth::user()->email);
        //     $okt->where('email', '=', Auth::user()->email);
        //     $nov->where('email', '=', Auth::user()->email);
        //     $des->where('email', '=', Auth::user()->email);

        //     $customer->where('email', '=', Auth::user()->email);
        //     $so->where('email', '=', Auth::user()->email);
        //     $value->where('email', '=', Auth::user()->email);
        //     $item->where('email', '=', Auth::user()->email);

        //     // dd($value);
        // }

        // if (Auth::user()->group_user == 'warehouse') {
        //     return view('dashboard.warehouse');
        // }
        

        // $jan      = $jan->sum('order_detail.qty_prepare');
        // $feb      = $feb->sum('order_detail.qty_prepare');
        // $mar      = $mar->sum('order_detail.qty_prepare');
        // $apr      = $apr->sum('order_detail.qty_prepare');
        // $mei      = $mei->sum('order_detail.qty_prepare');
        // $jun      = $jun->sum('order_detail.qty_prepare');
        // $jul      = $jul->sum('order_detail.qty_prepare');
        // $agu      = $agu->sum('order_detail.qty_prepare');
        // $sep      = $sep->sum('order_detail.qty_prepare');
        // $okt      = $okt->sum('order_detail.qty_prepare');
        // $nov      = $nov->sum('order_detail.qty_prepare');
        // $des      = $des->sum('order_detail.qty_prepare');
        // $customer = $customer->count('customer_id');
        // $so       = $so->count('order_id');
        // $value    = $value->sum('order_detail.total_release');
        // $item     = $item->sum('order_detail.qty_prepare');

        // $data = [
        //     '01' => $jan,
        //     '02' => $feb,
        //     '03' => $mar,
        //     '04' => $apr,
        //     '05' => $mei,
        //     '06' => $jun,
        //     '07' => $jul,
        //     '08' => $agu,
        //     '09' => $sep,
        //     '10' => $okt,
        //     '11' => $nov,
        //     '12' => $des,
        // ];

        // // dd($data);
        // return view('page.home.sales')->with([
        //     'data'     => $data,
        //     'customer' => $customer,
        //     'item'     => $item,
        //     'value'    => $value,
        //     'so'       => $so,
        // ]);
        return view('page.home.beranda');
    }

    public function error()
    {
        return view('page.home.home');
    }

    public function master()
    {
        return view('master');
    }

    public function configuration()
    {
        $akses = new AccessMenu();
        $akses->setData($akses->getData());
        return view('home.config');
    }

    public function permision()
    {
        return view('errors.permision');
    }

    public function query()
    {
        // $listing = FTP::connection()->getDirListing();
        // dd($listing);
        // dd(config('app.name'));
        // Config::write('system.name', 'http://xdlee.com');

//        $data = DB::table('actions')
        //                ->leftJoin('module_connection_action', 'actions.action_code', '=', 'module_connection_action.conn_ma_action')
        //                ->leftJoin('modules', 'module_connection_action.conn_ma_module', '=', 'modules.module_code')
        //                ->leftJoin('group_module_connection_module', 'group_module_connection_module.conn_gm_module', '=', 'modules.module_code')
        //                ->leftJoin('group_modules', 'group_modules.group_module_code', '=', 'group_module_connection_module.conn_gm_group_module')
        //                ->leftJoin('group_user_connection_group_module', 'group_user_connection_group_module.conn_gu_group_module', '=', 'group_modules.group_module_code')
        //                ->where('conn_gu_group_user', Auth::user()->group_user)
        //                ->orderBy('module_sort', 'asc')
        //                ->orderBy('action_sort', 'asc')
        //                ->toSql();
        //
        //        dd($data);
    }

}
