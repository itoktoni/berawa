<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Helper;
use Auth;

class District extends Model
{
    protected $table      = 'districts'; //nama table
    protected $primaryKey = 'subdistrict_id'; //nama primary key
    protected $guarded = [];
    public $searching    = 'subdistrict_name'; //default pencarian ketika di cari
    public $timestamps   = false; //kalau mau automatic update tanggal
    public $incrementing = true; //kalau id nya mau dibuatkan otomatis
    public $rules        = [ //validasi https://laravel.com/docs/5.5/validation
        'subdistrict_name'    => 'required|min:3',
    ];
    public $datatable;
    public static $autonumber; 
    public $status = [
        // 'ACTIVE' => 'ACTIVE',
        // 'PROSPECT' => 'PROSPECT',
        // 'INACTIVE' => 'INACTIVE',
    ];

    // const CREATED_AT = 'created_at';
    // const UPDATED_AT = 'updated_at';
    // protected $dates = [
    //     'created_at',
    //     'updated_at',
    // ];

   public function __construct($attributes = [])
    {
        parent::__construct($attributes); 
        $this->fillable = Helper::getTable($this->table);
        $this->datatable = [
            'subdistrict_id'      => [true => 'ID District'],
            'subdistrict_name'    => [true => 'District Name'],
        ];
    }

    protected function generateKey()
    {
        $autonumber = 'C' . date('Y') . date('m');
        return Helper::code($this->table, $this->primaryKey, $autonumber, config('website.autonumber'));
    }

    public static function boot()
    {
        // static::updating(function ($table) {
        //     $table->updated_by = Auth::user()->username;
        // });

        // static::saving(function ($table) {
        //     $table->created_by = Auth::user()->username;
        // });
    }

    public function simpan($request)
    {
        try
        {
            if(!$this->incrementing){
                $code = $this->generateKey();
                $request[$this->primaryKey] = $code;
            }
            $activity = $this->create($request);
            if ($activity->save()) {
                session()->put('success', 'Data Has Been Added !');
                return true;
            } 

            session()->put('danger', 'Data Failed To Save !');
            return false;
            
        } catch (\Illuminate\Database\QueryException $ex) {
            session()->put('danger', $ex->getMessage());
            return false;
        }
    }

    public function hapus($data)
    {
        if (!empty($data)) {
            $data = collect($data)->flatten()->all();
            try
            {
                $activity = $this->Destroy($data);
                if ($activity) {
                    session()->put('success', 'Data Has Been Deleted !');
                    return true;
                } 
                session()->flash('alert-danger', 'Data Can not Deleted !');
                return false;
            } catch (\Illuminate\Database\QueryException $ex) {
                session()->flash('alert-danger', $ex->getMessage());
            }
        }
    }

    public function ubah($id, $request)
    {
        try
        {
            $activity = $this->find($id)->update($request);
            if ($activity) {
                session()->put('success', 'Data Has Been Updated !');
            } 

            return $activity;

        } catch (\Illuminate\Database\QueryException $ex) {
            session()->put('danger', $ex->getMessage());
            return false;
        }
    }

    public function baca($id = null)
    {
        if (!empty($id)) {
            $data = $this->find($id);
            return $data;
        }

        return $this->select();
    }


    public function scopeCity($query, $flag)
    {
        $data = $query->where('city_id', $flag)->get();
        return $data;
    }

}
