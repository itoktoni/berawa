<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->string('id', 20)->default('')->primary('id');
			$table->string('code')->nullable();
			$table->string('grouping')->nullable();
			$table->string('category', 50)->nullable();
			$table->string('unit', 50)->nullable();
			$table->string('name')->index('name');
			$table->string('images', 20)->nullable();
			$table->float('harga_beli', 20, 3)->nullable();
			$table->float('harga_jual', 20, 3)->nullable();
			$table->longText('description')->nullable();
			$table->boolean('active')->nullable();
			$table->timestamps();
			$table->string('tag')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
