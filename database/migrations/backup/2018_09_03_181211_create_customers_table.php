<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public $tabel;

	public function __construct(){
		$this->tabel = 'customer';
	}

	public function up()
	{
		Schema::create($this->tabel, function(Blueprint $table)
		{
			$table->string($this->tabel.'_id', 20)->unique($this->tabel.'_id');
			$table->string($this->tabel.'_name')->nullable()->index($this->tabel.'_name');
			$table->string($this->tabel.'_phone', 20)->nullable();
			$table->string($this->tabel.'_contact')->nullable();
			$table->integer($this->tabel.'_status')->nullable();
			$table->string($this->tabel.'_email', 100)->nullable();
			$table->text($this->tabel.'_description', 65535)->nullable();
			$table->text($this->tabel.'_address', 65535)->nullable();
			
			$table->string('created_by')->nullable();
			$table->string('updated_by')->nullable();
			$table->string('email', 50)->nullable();
			$table->timestamps();

			$table->string('site_id', 50)->nullable();
			$table->integer('city_id')->nullable();
			$table->integer('province_id')->nullable();
			$table->integer('subdistrict_id')->nullable();

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop($this->tabel);
	}

}
