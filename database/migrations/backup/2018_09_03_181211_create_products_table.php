<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->string('product_id', 20)->default('')->primary('product_id');
			$table->string('product_code')->nullable();
			$table->string('product_grouping')->nullable();
			$table->string('product_category', 50)->nullable();
			$table->string('product_unit', 50)->nullable();
			$table->string('product_name')->index('product_name');
			$table->string('product_images', 20)->nullable();
			$table->float('product_harga_beli', 20, 3)->nullable();
			$table->float('product_harga_jual', 20, 3)->nullable();
			$table->longText('product_description')->nullable();
			$table->boolean('product_active')->nullable();
			$table->timestamps();
			$table->string('product_tag')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
