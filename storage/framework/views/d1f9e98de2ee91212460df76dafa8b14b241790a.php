<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header" style="border-bottom: 0.1px solid grey;">

        <a href="<?php echo e(route('home')); ?>" style="color:white;text-decoration: none;z-index: 999;">
            <div class="sidebar-title">
                <i style="margin-left:8px;font-size: 25px;margin-right: 7px;color:#ABB4BE;" class="fa fa-home" aria-hidden="true"></i>
                <span class="text-center" style="font-size: 13px;color:#ABB4BE;">Dashboard</span>
            </div>
        </a>

        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">

                <ul class="nav nav-main">

                    <?php if(isset($menu_list)): ?> 
                    <?php $__currentLoopData = $menu_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($menu->module_single == "1"): ?>
                    <li>
                        <a href="<?php echo e($menu->module_link); ?>">
                            <!--<span class="pull-right label label-primary">182</span>-->
                            <i class="fa fa-external-link" aria-hidden="true"></i>
                            <span><?php echo e($menu->module_name); ?> </span>
                        </a>
                    </li>
                    <?php else: ?>
                    <?php if($menu->module_visible == '1' && $menu->module_enable == '1'): ?>
                    <li class="nav-parent <?php echo e(Request::segment(2) === $menu->module_link ? 'nav-expanded nav-active' : ''); ?>">
                        <a>
                            <i class="fa fa-tasks" aria-hidden="true"></i>
                            <span class="text-capitalize"> <?php echo e($menu->module_name); ?></span>
                        </a>
                        <ul class="nav nav-children">
                            <?php $__currentLoopData = $action_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data_action): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($menu->module_code === $data_action->module_code && $data_action->action_visible == '1'): ?>
                            <li<?php echo e($data_action->action_function === Request::segment(3) && Request::segment(2) === $menu->module_link ? " class=nav-active" : ''); ?>>
                                <a href="<?php echo route("$data_action->action_code"); ?>">
                                    <?php echo e($data_action->action_name); ?>

                                </a>
                            </li>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </li>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </ul>
            </nav>
            <hr class="separator" />


        </div>

    </div>

</aside>