<!doctype html>
<html class="scroll">
<head>
    <?php echo $__env->make('backend.'.config('website.backend').'.layouts.meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('backend.'.config('website.backend').'.layouts.script', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <script type="text/javascript">
        var start_time = +new Date();
        window.onload = function() {
            var time = ((+new Date() - start_time) / 1000);
            var data = time + ' Second';
            document.getElementById("pageload").textContent=data;
        };
    </script>
</head>
<body>

    <section class="body">
        <?php echo $__env->make('backend.'.config('website.backend').'.layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="inner-wrapper">
            <?php echo $__env->make('backend.'.config('website.backend').'.layouts.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <section role="main" class="content-body">
                <header class="page-header">
                    <span class="col-lg-11 col-sm-6 col-xl-3 pull-left" style="color:#A6A3A3;margin-top:15px;z-index: 1;">
                        <marquee>
                            <div style="float: left;">Page Load :&ensp;</div>
                            <div id="pageload" style="float:left;"></div>
                            <?php
                            function echo_memory_usage()
                            {
                                $kata = ", Memory Usage ";
                                $mem_usage = memory_get_usage(true);
                                if($mem_usage < 1024)
                                    echo $kata.$mem_usage." bytes";
                                elseif($mem_usage < 1048576)
                                    echo $kata.round($mem_usage / 1024, 2)." KB";
                                else
                                    echo $kata.round($mem_usage / 1048576, 2)." MB";
//                                    echo "<br/>";
                            }

                            echo_memory_usage();
                            ?>
                        </marquee>
                    </span>

                    <div class="right-wrapper pull-right">       
                        <a class="sidebar-right-toggle" data-open="sidebar-right">
                            <i style="margin-right: -30px;" class="fa fa-paper-plane"></i>
                        </a>
                    </div>
                </header>
                <!-- start: page -->
                <?php echo $__env->yieldContent('content'); ?>
                <div style="padding-bottom: 30px;"></div>
                <!-- end: page -->
            </section>
        </div>

        <?php echo $__env->make('backend.'.config('website.backend').'.layouts.right', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </section>

    <script src="<?php echo e(Helper::backend('javascripts/theme.js')); ?>" ></script>
    <script src="<?php echo e(Helper::backend('javascripts/theme.init.js')); ?>"></script>
    <script>

        $(function() {
            // $(".datepicker").datepicker({dateFormat: 'yy-mm-dd'});
            // $(".datepicker1").datepicker({dateFormat: 'yy-mm-dd'});
            // $(".datepicker2").datepicker({dateFormat: 'yy-mm-dd'});
            // $(".datepicker3").datepicker({dateFormat: 'yy-mm-dd'});
            // $(".datepicker4").datepicker({dateFormat: 'yy-mm-dd'});
            // $(".datepicker5").datepicker({dateFormat: 'yy-mm-dd'});
            // $(".option").select2();
            // $(".option1").select2();
            // $(".option2").select2();
            // $(".option3").select2();
            // $(".option4").select2();
            // $(".option5").select2();
            // $("#option").select2();
            // $("#option1").select2();
            // $("#option2").select2();
            // $("#option3").select2();
            // $("#option4").select2();
            // $("#option5").select2();
            // $("#option6").select2();

            $('.modal').magnificPopup({
                type: 'inline',
                preloader: false,
                modal: true
            });

             // $('.summernote').summernote();
             
            $('.summernote').summernote({
              minHeight: 100,  
              toolbar: [
                // [groupName, [list of button]]
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['style', ['bold', 'italic', 'underline', 'clear']],
              ]
            });
        });
    </script>
    <?php echo $__env->yieldContent('javascript'); ?>
    <?php echo $__env->make('backend.'.config('website.backend').'.layouts.notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</body>
</html>