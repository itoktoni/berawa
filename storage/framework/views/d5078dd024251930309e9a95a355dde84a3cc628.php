<?php if(Session::has('success')): ?>
<script type="text/javascript">
    $(function() {
    new PNotify({
    title: 'Notification Success',
            text: '<?php echo e(Session::get("success")); ?>',
            type: 'success'
    });
    <?php echo e(session()->forget('success')); ?>

    });</script>
<?php endif; ?>

<?php if(Session::has('primary')): ?>
<script type="text/javascript">
            $(function() {
            new PNotify({
            title: 'For Your Information !',
                    text: '<?php echo e(Session::get("primary")); ?>',
                    type: 'primary',
                    addclass: 'notification-primary',
                    icon: 'fa fa-twitter'
                    });
            <?php echo e(session()->forget('primary')); ?>

            });</script>
<?php endif; ?>

<?php if(Session::has('dark')): ?>
<script type="text/javascript">
            $(function() {
            new PNotify({
            title: 'Regular Notice !',
                    text: '<?php echo e(Session::get("dark")); ?>',
                    addclass: 'notification-dark',
                    icon: 'fa fa-user'
                    });
            <?php echo e(session()->forget('dark')); ?>

            });</script>
<?php endif; ?>

<?php if(Session::has('danger')): ?>
<script type="text/javascript">
            $(function() {
            new PNotify({
            title: 'Notification Failed !',
                    text: '<?php echo e(Session::get("danger")); ?>',
                    type: 'error',
                    hide: false
                    });
            <?php echo e(session()->forget('danger')); ?>

            });</script>
<?php endif; ?>

<?php if(Session::has('sticky')): ?>
<script type="text/javascript">
            $(function() {
            new PNotify({
            title: 'Notification Failed !',
                    text: '<?php echo e(Session::get("sticky")); ?>',
                    type: 'error',
                    hide: false
                    });
            <?php echo e(session()->forget('sticky')); ?>

            });</script>
<?php endif; ?>

<?php if(Session::has('warning')): ?>
<script type="text/javascript">
            $(function() {
            new PNotify({
            title: 'Notification Warning !',
                    text: '<?php echo e(Session::get("warning")); ?>'
                    });
            <?php echo e(session()->forget('warning')); ?>

            });</script>
<?php endif; ?>

<?php if($errors->any()): ?>
<script type="text/javascript">
            $(function() {
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    new PNotify({
                    title: 'Validation Error !',
                            text: '<?php echo e($error); ?>',
                            type: 'error',
                            hide: false
                            });
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            });</script>
<?php endif; ?>

<?php if(Session::has('alert-primary')): ?>
<script type="text/javascript">
            $(function() {
            new PNotify({
            title: 'For Your Information !',
                    text: '<?php echo e(Session::get("alert-primary")); ?>',
                    type: 'primary',
                    addclass: 'notification-primary',
                    icon: 'fa fa-twitter'
                    });
            });</script>
<?php endif; ?>

<?php if(Session::has('alert-dark')): ?>
<script type="text/javascript">
            $(function() {
            new PNotify({
            title: 'Regular Notice !',
                    text: '<?php echo e(Session::get("alert-dark")); ?>',
                    addclass: 'notification-dark',
                    icon: 'fa fa-user'
                    });
            });</script>
<?php endif; ?>

<?php if(Session::has('alert-danger')): ?>
<script type="text/javascript">
            $(function() {
            new PNotify({
            title: 'Notification Failed !',
                    text: '<?php echo e(Session::get("alert-danger")); ?>',
                    type: 'error',
                    hide: false
                    });
            });</script>
<?php endif; ?>

<?php if(Session::has('alert-success')): ?>
<script type="text/javascript">
            $(function() {
            new PNotify({
            title: 'Notification Success',
                    text: '<?php echo e(Session::get("alert-success")); ?>',
                    type: 'success'
                    });
            });</script>
<?php endif; ?>

<?php if(Session::has('alert-warning')): ?>
<script type="text/javascript">
            $(function() {
            new PNotify({
            title: 'Notification Warning !',
                    text: '<?php echo e(Session::get("alert-warning")); ?>'
                    });
            });
</script>
<?php endif; ?>


