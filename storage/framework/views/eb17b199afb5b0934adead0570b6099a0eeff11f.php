<?php $__env->startSection('content'); ?>

<?php $__env->startSection('javascript'); ?>

<script>
    $(function() {

        $('#province').change(function() {
            var id = $("#province option:selected").val();
            $('#city').select2({
                placeholder: 'Select an City',
                ajax: {
                        url: '<?php echo e(route("city-api")); ?>',
                        dataType: 'json',
                        data: function (params) {
                            return {
                            q: params.term, // search term
                            province: id,
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }   
            });
        });

    });
</script>
<?php $__env->stopSection(); ?>


<?php echo Form::open(['route' => 'website', 'class' => 'form-horizontal', 'files' => true]); ?>  

<div class="row">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title text-right">Configuration ( <?php echo e(config('website.name')); ?> )</h2>
        </header>
        <div class="panel-body">

            <div class="col-md-4 col-lg-3">
                <section class="panel">
                    <div class="">
                        <div class="text-center">
                            <?php if(empty(config('website.favicon'))): ?>
                            <img src="<?php echo e(Helper::asset('/files/logo/default_logo.png')); ?>" class="img-thumbnail rounded center-block img-responsive">
                            <?php else: ?>
                            <img src="<?php echo e(Helper::asset('/files/logo/'.config('website.favicon'))); ?>" class="img-thumbnail rounded center-block img-responsive">
                            <?php endif; ?>
                        </div>
                    </div>
                </section>

                <h5 class="text-center">Favicon :<code>W: 50px & H: 50px  </code></h5>
                <hr class="dotted short">

                <div class="col-md-12">
                    <input type="file" name="favicon" class="btn btn-default btn-sm btn-block">
                </div>
                <br>
                <br>
                <hr>
                <section class="panel">
                    <div class="">
                        <div class="text-center">
                            <?php if(empty(config('website.logo'))): ?>
                            <img src="<?php echo e(Helper::asset('/files/logo/default_logo.png')); ?>" class="img-thumbnail rounded center-block img-responsive" alt="no profile">
                            <?php else: ?>
                            <img src="<?php echo e(Helper::asset('/files/logo/'.config('website.logo'))); ?>" class="img-thumbnail rounded center-block img-responsive" alt="John Doe">
                            <?php endif; ?>
                        </div>
                    </div>
                </section>

                <h5 class="text-center">Logo <code>W: 50px-100px & H: 230px</code></h5>
                <hr class="dotted short">

                <div class="col-md-12">
                    <input type="file" name="logo" class="btn btn-default btn-sm btn-block">
                </div>

            </div>
            <div class="col-md-8 col-lg-9">

                <div class="panel-group" id="">
                    <div class="panel panel-accordion">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1One">
                                    Application
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1One" class="accordion-body collapse in">
                            <div class="panel-body">
                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Website Name</label>
                                        <div class="col-md-4">
                                            <input type="text" name="name" value="<?php echo e(config('website.name')); ?>" required="" class="form-control">
                                        </div>

                                        <label class="col-md-2 control-label">Environment</label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="env">
                                                <option <?php echo e(config('app.env') == 'local' ? 'selected=selected' : ''); ?> value="local">Local</option>
                                                <option <?php echo e(config('app.env') == 'production' ? 'selected=selected' : ''); ?> value="production">Production</option>
                                            </select>
                                        </div>

                                       
                                        <label class="col-md-2 control-label">Show Setting</label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="image">
                                                <?php $__currentLoopData = $group; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $g): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($g->group_user_code); ?>"><?php echo e($g->group_user_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                        <label class="col-md-2 control-label">Database Driver</label>
                                        <div class="col-md-4">
                                            <?php echo e(Form::select('DB_CONNECTION', $database_driver, config('database.default'), ['placeholder' => 'Please Select Database', 'class' => 'form-control'])); ?>

                                        </div>
                                      
                                        <label class="col-md-2 control-label">Phone</label>
                                        <div class="col-md-4">
                                            <input type="text" value="<?php echo e(config('website.phone')); ?>" name="phone" class="form-control">
                                        </div>

                                        <label class="col-md-2 control-label">Email Pengirim</label>
                                        <div class="col-md-4">
                                            <input type="text" value="<?php echo e(config('mail.from.name')); ?>" name="mail_name" class="form-control">
                                        </div>

                                        <label class="col-md-2 control-label">Nama Owner</label>
                                        <div class="col-md-4">
                                            <input type="text" value="<?php echo e(config('website.owner')); ?>" name="owner" class="form-control">
                                        </div>
                                        <label class="col-md-2 control-label">Alamat Email</label>
                                        <div class="col-md-4">
                                            <input type="text" value="<?php echo e(config('mail.from.address')); ?>" name="mail_address" class="form-control">
                                        </div> 

                                        <hr>
                                        <label class="col-md-2 control-label">Address</label>
                                        <div class="col-md-10">
                                            <textarea class="form-control" rows="3" name="address" cols="50"><?php echo e(config('website.address')); ?></textarea>
                                        </div>

                                        <hr>
                                        <label class="col-md-2 control-label">Description</label>
                                        <div class="col-md-10">
                                            <textarea class="form-control summernote" rows="3" name="description" cols="50"><?php echo e(config('website.description')); ?></textarea>
                                        </div>
                                        
                                        <hr>
                                    </div>  

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-accordion">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1Five">
                                    Management Template
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1Five" class="accordion-body collapse">
                            <div class="panel-body">

                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Backend</label>
                                        <div class="col-md-4">
                                            <?php echo e(Form::select('backend', $backend, config('website.backend'), ['placeholder' => 'Please Select Template', 'class' => 'form-control'])); ?>

                                        </div>

                                        <label class="col-md-2 control-label">Frontend</label>
                                        <div class="col-md-4">
                                            <?php echo e(Form::select('frontend', $frontend, config('website.frontend'), ['placeholder' => 'Please Select Template', 'class' => 'form-control'])); ?>

                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-accordion">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1Two">
                                    Debug Error
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1Two" class="accordion-body collapse">
                            <div class="panel-body">

                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Debug</label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="live">
                                                <option value="1">Live</option>
                                                <option value="0">Maintenance</option>
                                                <option value="2">Under Construction</option>
                                            </select>
                                        </div>

                                        <label class="col-md-2 control-label">Debug Bar</label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="debug">
                                                <option <?php echo e(config('app.debug') == '1' ? 'selected=selected' : ''); ?> value="1">Show DebugBar</option>
                                                <option <?php echo e(config('app.debug') == '0' ? 'selected=selected' : ''); ?> value="0">Hidden DebugBar</option>
                                            </select>
                                        </div>

                                        
                                    </div> 

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="panel panel-accordion">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#address">
                                    Cache and Session
                                </a>
                            </h4>
                        </div>
                        <div id="address" class="accordion-body collapse">
                            <div class="panel-body">
                                <div class="col-md-12"> 

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Session</label>
                                        <div class="col-md-4">
                                            <?php echo e(Form::select('session', $session_driver, config('session.driver'), ['placeholder' => 'Please Select Session', 'class' => 'form-control'])); ?>

                                        </div>
                                        <label class="col-md-2 control-label">Cache</label>
                                        <div class="col-md-4">
                                            <?php echo e(Form::select('cache', $cache_driver, config('cache.default'), ['placeholder' => 'Please Select Cache', 'class' => 'form-control'])); ?>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Session Expire</label>
                                        <div class="col-md-4">
                                           <input type="text" name="website_session" value="<?php echo e(config('website.session')); ?>" class="form-control">
                                        </div>
                                        <label class="col-md-2 control-label">Cache Expire</label>
                                        <div class="col-md-4">
                                             <input type="text" name="website_cache" value="<?php echo e(config('website.cache')); ?>" class="form-control">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-accordion">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1Three">
                                    Database Configuration
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1Three" class="accordion-body collapse">
                            <div class="panel-body">
                                <div class="col-md-12"> 
                                     <div class="form-group">

                                        <label class="col-md-2 control-label">
                                            Database Name
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" name="DB_DATABASE" value="<?php echo e(env('DB_DATABASE')); ?>" class="form-control">
                                        </div>
                                        <label class="col-md-2 control-label">
                                            Database Host
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" name="DB_HOST" value="<?php echo e(env('DB_HOST')); ?>" class="form-control">
                                        </div>
                                        <label class="col-md-2 control-label">      Username
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" name="DB_USERNAME" value="<?php echo e(env('DB_USERNAME')); ?>" class="form-control">
                                        </div>
                                        <label class="col-md-2 control-label">      Password
                                        </label>
                                        <div class="col-md-4">
                                            <input type="password" name="DB_PASSWORD" value="<?php echo e(env('DB_PASSWORD')); ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="panel panel-accordion">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1Four">
                                    Email Configuration
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1Four" class="accordion-body collapse">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                         Mail Driver
                                        </label>
                                        <div class="col-md-4">
                                            <?php echo e(Form::select('MAIL_DRIVER', $mail_driver, config('mail.driver'), ['placeholder' => 'Please Select Template', 'class' => 'form-control'])); ?>

                                        </div>

                                        <label class="col-md-2 control-label">
                                            Mail Host
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" name="MAIL_HOST" value="<?php echo e(config('mail.host')); ?>" class="form-control">
                                        </div>
                                        <label class="col-md-2 control-label">
                                            Mail Port
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" name="MAIL_PORT" value="<?php echo e(config('mail.port')); ?>" class="form-control">
                                        </div>
                                        <label class="col-md-2 control-label">      Encryption
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" name="MAIL_ENCRYPTION" value="<?php echo e(config('mail.driver')); ?>" class="form-control">
                                        </div>
                                        <label class="col-md-2 control-label">      Username
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" name="MAIL_USERNAME" value="<?php echo e(config('mail.driver')); ?>" class="form-control">
                                        </div>
                                        <label class="col-md-2 control-label">      Password
                                        </label>
                                        <div class="col-md-4">
                                            <input type="password" name="MAIL_PASSWORD" value="<?php echo e(config('mail.driver')); ?>" class="form-control">
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-fixed-bottom" id="menu_action">
            <div class="text-right" style="padding:5px">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>
    </section>
</div>

<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.'.config('website.backend').'.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>