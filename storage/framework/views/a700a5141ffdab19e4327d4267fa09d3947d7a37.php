<!doctype html>
<html class="fixed">
<head>

    <meta charset="UTF-8">
    <meta name="keywords" content="<?php echo e(config('app.name', 'Laravel')); ?>" />
    <meta name="description" content="<?php echo e(config('app.name', 'Laravel')); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('website.name', 'Laravel')); ?></title>
    <link href="<?php echo e(Helper::files('logo/'.config('website.favicon','default_favicon.png'))); ?>" rel="shortcut icon">

    <link rel="stylesheet" href="<?php echo e(Helper::backend('vendor/bootstrap/css/bootstrap.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(Helper::backend('vendor/font-awesome/css/font-awesome.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(Helper::backend('vendor/magnific-popup/magnific-popup.css')); ?>" />

    <link rel="stylesheet" href="<?php echo e(Helper::backend('stylesheets/theme.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(Helper::backend('stylesheets/skins/default.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(Helper::backend('stylesheets/theme-custom.css')); ?>">
    <script src="<?php echo e(Helper::backend('vendor/modernizr/modernizr.js')); ?>"></script>

    <script> window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token(),]);?></script>

</head>
<body>
<section class="body-sign">
    <div class="center-sign">
       
    <?php echo $__env->yieldContent('content'); ?>
    </div>
</section>

<!-- Vendor -->
<script src="<?php echo e(Helper::backend('vendor/jquery/jquery.js')); ?>"></script>
<script src="<?php echo e(Helper::backend('vendor/jquery-browser-mobile/jquery.browser.mobile.js')); ?>"></script>
<script src="<?php echo e(Helper::backend('vendor/bootstrap/js/bootstrap.js')); ?>"></script>
<script src="<?php echo e(Helper::backend('vendor/nanoscroller/nanoscroller.js')); ?>"></script>
<script src="<?php echo e(Helper::backend('vendor/jquery-placeholder/jquery.placeholder.js')); ?>"></script>

</body>
</html>