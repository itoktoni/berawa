
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<meta name="csrf-token" value="<?php echo e(csrf_token()); ?>">
<title><?php echo e(config('app.name')); ?></title>
<link type="image/x-icon" href="<?php echo e(Helper::files('logo/'.config('website.favicon','default_favicon.png'))); ?>" rel="shortcut icon">
 <link href="<?php echo e(Helper::files('logo/'.config('website.favicon','default_favicon.png'))); ?>" rel="shortcut icon">
<meta name="keywords" content="<?php echo e(config('app.name')); ?>" />
<meta name="description" content="<?php echo e(config('app.name')); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="stylesheet" href="<?php echo e(Helper::backend('vendor/bootstrap/css/bootstrap.min.css')); ?>"/>
<link rel="stylesheet" href="<?php echo e(Helper::backend('vendor/font-awesome/css/font-awesome.min.css')); ?>"/>
<link rel="stylesheet" href="<?php echo e(Helper::backend('vendor/pnotify/pnotify.custom.css')); ?>" />
<link rel="stylesheet" href="<?php echo e(Helper::backend('vendor/jquery-ui/css/jquery-ui.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(Helper::backend('vendor/select/select2.css')); ?>">
<link rel="stylesheet" href="<?php echo e(Helper::backend('vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')); ?>">
<link rel="stylesheet" href="<?php echo e(Helper::backend('vendor/magnific-popup/magnific-popup.css')); ?>">
<link rel="stylesheet" href="<?php echo e(Helper::backend('vendor/jquery-typeahead/jquery.typeahead.min.css')); ?>" />

<link rel="stylesheet" href="<?php echo e(Helper::backend('stylesheets/theme.min.css')); ?>"/>
<link rel="stylesheet" href="<?php echo e(Helper::backend('stylesheets/skins/default.min.css')); ?>"/>
<link rel="stylesheet" href="<?php echo e(Helper::backend('vendor/summernote/summernote-bs4.css')); ?>" />

<link rel="stylesheet" href="<?php echo e(Helper::backend('stylesheets/theme-custom.css')); ?>">
<?php echo $__env->yieldContent('css'); ?>

