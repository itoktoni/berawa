
<aside id="sidebar-right" class="sidebar-right">
    <div class="nano">
        <div class="nano-content">
            <a href="#" class="mobile-close visible-xs">
                Close All Module
            </a>
            <div class="sidebar-right-wrapper">
                <div class="sidebar-widget widget-calendar">
                    <div>
                        <div class="btn btn-default btn-lg btn-block">
                            Access Module 
                        </div> 
                    </div>
                    <ul style="margin-top: -15px;">
                        <?php 
                        $access = Auth::User()->username.'_group_access'; 
                        ?>
                        <?php if(isset($group_list)): ?>
                        <?php $__currentLoopData = $group_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li>
                            <div class="col-xs-12">
                                <a onclick="location.href = '<?php echo e(route('access_group',[$group->group_module_code])); ?>';" class="mb-xs mt-xs mr-xs btn btn-<?php echo e((Session($access) == $group->group_module_code ? 'primary' : 'default')); ?> btn-block">
                                    <h6 class="text-right text-dark<?php echo e((Session($access) == $group->group_module_code ? '-inverse' : '')); ?>"> <?php echo e($group->group_module_name); ?></h6>
                                </a>
                            </div>
                        </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php if(Auth::user()->group_user == 'developer'): ?>
                        <li>
                            <div class="col-xs-12">
                                <a onclick="location.href = '<?php echo e(route('reboot')); ?>';" class="mb-xs mt-xs mr-xs btn btn-danger btn-block">
                                    <h6 class="text-right text-dark-inverse"> Reboot </h6>
                                </a>
                            </div>
                        </li>
                        <?php endif; ?>
                        <?php endif; ?>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</aside>