<?php $__env->startSection('content'); ?>

 <!-- Home -->

  <div class="home">
    
    <!-- Home Slider -->

    <div class="home_slider_container">
      
      <div class="owl-carousel owl-theme home_slider">
        <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <!-- Slider Item -->
        <div class="owl-item home_slider_item">
          <div class="home_slider_background" style="background-image:url(<?php echo e(Helper::files('slider/'.$s->images)); ?>)"></div>

          <div class="home_slider_content text-center">
            <div class="home_slider_content_inner" data-animation-in="flipInX" data-animation-out="animate-out fadeOut">
              <?php echo $s->description; ?>

              <div class="button home_slider_button"><div class="button_bcg"></div><a href="#">explore now<span></span><span></span><span></span></a></div>
            </div>
          </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        
      </div>
      
      <!-- Home Slider Nav - Prev -->
      <div class="home_slider_nav home_slider_prev">
        <svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          width="28px" height="33px" viewBox="0 0 28 33" enable-background="new 0 0 28 33" xml:space="preserve">
          <defs>
            <linearGradient id='home_grad_prev'>
              <stop offset='0%' stop-color='#fa9e1b'/>
              <stop offset='100%' stop-color='#8d4fff'/>
            </linearGradient>
          </defs>
          <path class="nav_path" fill="#F3F6F9" d="M19,0H9C4.029,0,0,4.029,0,9v15c0,4.971,4.029,9,9,9h10c4.97,0,9-4.029,9-9V9C28,4.029,23.97,0,19,0z
          M26,23.091C26,27.459,22.545,31,18.285,31H9.714C5.454,31,2,27.459,2,23.091V9.909C2,5.541,5.454,2,9.714,2h8.571
          C22.545,2,26,5.541,26,9.909V23.091z"/>
          <polygon class="nav_arrow" fill="#F3F6F9" points="15.044,22.222 16.377,20.888 12.374,16.885 16.377,12.882 15.044,11.55 9.708,16.885 11.04,18.219 
          11.042,18.219 "/>
        </svg>
      </div>
      
      <!-- Home Slider Nav - Next -->
      <div class="home_slider_nav home_slider_next">
        <svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        width="28px" height="33px" viewBox="0 0 28 33" enable-background="new 0 0 28 33" xml:space="preserve">
          <defs>
            <linearGradient id='home_grad_next'>
              <stop offset='0%' stop-color='#fa9e1b'/>
              <stop offset='100%' stop-color='#8d4fff'/>
            </linearGradient>
          </defs>
        <path class="nav_path" fill="#F3F6F9" d="M19,0H9C4.029,0,0,4.029,0,9v15c0,4.971,4.029,9,9,9h10c4.97,0,9-4.029,9-9V9C28,4.029,23.97,0,19,0z
        M26,23.091C26,27.459,22.545,31,18.285,31H9.714C5.454,31,2,27.459,2,23.091V9.909C2,5.541,5.454,2,9.714,2h8.571
        C22.545,2,26,5.541,26,9.909V23.091z"/>
        <polygon class="nav_arrow" fill="#F3F6F9" points="13.044,11.551 11.71,12.885 15.714,16.888 11.71,20.891 13.044,22.224 18.379,16.888 17.048,15.554 
        17.046,15.554 "/>
        </svg>
      </div>

      <!-- Home Slider Dots -->
      
    </div>

  </div>

  <!-- Intro -->
  
  <div id="offer" class="intro">
    <div class="container">
      <div class="row">
        <div class="col">
          <h2 class="intro_title text-center">Paket Membership</h2>
        </div>
      </div>
      <!-- <div class="row">
        <div class="col-lg-10 offset-lg-1">
          <div class="intro_text text-center">
            <p>BRW HOLIDAY menyediakan beberapa paket membership yang bisa kamu ambil, Ayo segera berlangganan.</p>
          </div>
        </div>
      </div> -->
      <div class="row intro_items">

        <!-- Intro Item -->
        
        <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-lg-4 intro_col">
          <div data="<?php echo e($p->id); ?>" class="intro_item">
            <div class="intro_item_overlay"></div>
            <div class="intro_item_background" style="background-image:url(<?php echo Helper::files('product/'.$p->images); ?>)"></div>
            <div class="intro_item_content d-flex flex-column align-items-center justify-content-center">
              
              <div class="button intro_button"><div class="button_bcg"></div><a href="#">Select</a></div>
              <div class="intro_center text-center">
                <h1><?php echo e($p->name); ?></h1>
                <div style="margin-top:40px;clear:both;background-color: <?php echo e($p->name != 'Platinum' ? $p->name  : '#fa9e1c'); ?>;" class="intro_date">From <?php echo e(number_format($p->harga_jual)); ?></div>
                <div class="rating rating_4">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <!-- Intro Item -->

      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col">
        <h2 class="intro_title text-center">Data Diri</h2>
      </div>
    </div>
    <hr style="margin-top: 50px !important;">
    <?php echo Form::open(['route' => 'process','id' => 'form', 'class' => 'form-horizontal', 'files' => true]); ?>  
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" tabindex="1" value="example" class="form-control" name="name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter name">
                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Address</label>
                    <textarea class="form-control" name="address">test address</textarea>
                </div>
                
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" tabindex="2" class="form-control" value="demo@demo.com" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Phone</label>
                    <input type="text" tabindex="3" class="form-control" name="phone" value="123456789" id="exampleInputPassword1" placeholder="Enter Phone">
                </div>
            </div>
            <hr>
            <div class="col-md-12">
                <span id="submit" class="btn btn-primary pull-right">Submit</span>
            </div>
            <input id="token" type="hidden" name="token">
        </div>
    <?php echo Form::close(); ?>

  </div>
<hr>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-PRD4xTzS7TzEa5Yn"></script>

<script type="text/javascript">
   $(".intro_items .intro_item").click(function(){
        $(".intro_items .intro_item").removeAttr('id');
        $(this).attr('id', 'active');
        var id = $(this).attr('data');
        // alert(id);
        $.ajax({
            url: "<?php echo e(route('process')); ?>",
            type: "POST",
            data: $('#form').serialize()+ "&data=" + id ,
            success: function (response) {
                $('#token').val(response);
            },
            error: function(jqXHR, textStatus, errorThrown) {
              notif(textStatus);
            }
        });
    });

   $('#submit').click(function(e){
        id = $('#token').val();
        snap.pay($('#token').val());
        e.preventDefault();
    });


 function notif(data,error=true){
    $.amaran({
        inEffect  :'slideRight',
        outEffect :'slideLeft',
        position          :'top right',
        theme:'colorful',
        content:{
            bgcolor: error ? '#da4f49' : '#27ae60',
            color:'#fff',
            message: data,
           },
    });
}
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.'.config('website.frontend').'.layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>