<?php $__env->startSection('content'); ?>

<div class="panel panel-sign">
    <div class="panel-title-sign">
       <h3><?php echo e(config('website.name')); ?></h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" role="form" method="POST" action="<?php echo e(route('login')); ?>">
            <?php echo e(csrf_field()); ?>

            <div class="form-group mb-lg <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                <label class="col-md-3 col-xs-3 text-right control-label" for="inputDefault">Email</label>
                <div class="col-md-9 col-xs-9">
                    <input name="email" type="email" placeholder="masukan email" class="form-control" value="<?php echo e(old('email')); ?>" required autofocus/>
                    
                </div>
            </div>
            <div class="form-group mb-lg <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                <label class="col-md-3 col-xs-3 text-right control-label" for="inputDefault">Password</label>
                <div class="col-md-9 col-xs-9">
                    <input name="password" placeholder="masukan password" type="password" class="form-control" />
                    
                    <?php if($errors->has('email')): ?>
                    <span class="help-block">
                        <strong><?php echo e($errors->first('email')); ?></strong>
                    </span>
                    <?php endif; ?>
                    
                    <?php if($errors->has('password')): ?>
                    <span class="help-block">
                        <strong><?php echo e($errors->first('password')); ?></strong>
                    </span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="checkbox-custom checkbox-default">
                        <input type="checkbox" name="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>
                               <label for="RememberMe">Remember Me</label>
                    </div>
                </div>
                <div class="col-sm-4 text-right">
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
            </div>
        </form>
        <hr>
        <a style="margin-top: -20px;margin-left: -25px;margin-bottom: -10px;" class="btn text-danger btn-link text-center" href="<?php echo e(url('/password/reset')); ?>">
            Lupa Password ?
        </a>
    </div>
</div>

<?php $__env->stopSection(); ?>           
<?php echo $__env->make('auth.credential', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>