<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('frontend.'.config('website.frontend').'.meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldContent('css'); ?>
<body>
<div class="super_container">
  
  <!-- Header -->

  <header class="header">

    <!-- Main Navigation -->

    <nav class="main_nav">
      <div class="container">
        <div class="row">
          <div class="col main_nav_col d-flex flex-row align-items-center justify-content-start">
            <div class="logo_container">
              <div class="logo">
                <a href="<?php echo e(route('beranda')); ?>#">
                  <img src="<?php echo e(Helper::files('logo/'.config('website.logo'))); ?>" alt="">
                </a>
              </div>
            </div>
            <div class="main_nav_container ml-auto">
              <ul class="main_nav_list">
                <li class="main_nav_item"><a href="<?php echo e(route('beranda')); ?>#">home</a></li>
                <li class="main_nav_item"><a href="<?php echo e(route('beranda')); ?>#about">about us</a></li>
                <li class="main_nav_item"><a href="<?php echo e(route('beranda')); ?>#offer">offers</a></li>
                <li class="main_nav_item"><a href="<?php echo e(route('contact')); ?>">contact</a></li>
                <li class="main_nav_item"><a style="color: #b3585e;" href="<?php echo e(route('member')); ?>">join member</a></li>
              </ul>
            </div>

            <div class="hamburger">
              <i class="fa fa-bars trans_200"></i>
            </div>
          </div>
        </div>
      </div>  
    </nav>

  </header>

  <div class="menu trans_500">
    <div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
      <div class="menu_close_container"><div class="menu_close"></div></div>
      <div class="logo menu_logo"><a href="<?php echo e(route('beranda')); ?>#"></a></div>
      <ul>
        <li class="menu_item"><a href="<?php echo e(route('beranda')); ?>#">home</a></li>
        <li class="menu_item"><a href="<?php echo e(route('beranda')); ?>#">about us</a></li>
        <li class="menu_item"><a href="<?php echo e(route('beranda')); ?>#offer">offers</a></li>
        <li class="menu_item"><a href="<?php echo e(route('contact')); ?>">contact</a></li>
        <li class="menu_item"><a style="color: #b3585e;" href="<?php echo e(route('member')); ?>">join member</a></li>
      </ul>
    </div>
  </div>

  <?php echo $__env->yieldContent('content'); ?>

  <?php echo $__env->make('frontend.'.config('website.frontend').'.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</div>
<?php echo $__env->make('frontend.'.config('website.frontend').'.script', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldContent('js'); ?>
</body>
</html>